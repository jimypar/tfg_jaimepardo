package gui;

import cliente.ControladorCliente;
import com.jaimepardo.mapaDB.*;
import util.Util;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, ChangeListener, MouseListener, PropertyChangeListener, DocumentListener {
    private Vista vista;
    private ControladorCliente controladorCliente;
    private int puntos;
    private int deporteSelected;
    private Usuario usuario;
    private Evento eventoActual;


    /**
     * Constructor del controlador
     *
     * @param vista              recibe la vista de la que toma los datos
     * @param controladorCliente recibe el controlador que usa para comunicarse con
     *                           el servidor
     */
    public Controlador(Vista vista, ControladorCliente controladorCliente) {

        this.vista = vista;
        this.controladorCliente = controladorCliente;

        refrescarUsuario();

        addActionListeners(this);
        addChangeListeners(this);
        addMouseListeners(this);
        addListSelectionListeners(this);
        addItemListeners(this);
        addPropertyChangeListeners(this);
        addDocumentListeners(this);

    }

    /**
     * Metodo que refresca los datos de un usuario cuando algun dato de este cambia
     */
    private void refrescarUsuario() {


        usuario = controladorCliente.getUsuario(controladorCliente.sessionUser.getId());
        puntos = usuario.getPuntos();
        vista.miPerfilButton.setText("Mi Perfil: " + usuario.getUsername());
        vista.userPuntosTXT.setText(usuario.getPuntos() + " puntos");
        crearApuestasActuales();

    }


    /**
     * Añade los listeners a los botones y les asigna un nombre
     *
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {

        vista.usuarioAddButton.addActionListener(this);
        vista.usuarioAddButton.setActionCommand("usuarioAdd");
        vista.usuarioModButton.addActionListener(this);
        vista.usuarioModButton.setActionCommand("usuarioMod");
        vista.usuarioDelButton.addActionListener(this);
        vista.usuarioDelButton.setActionCommand("usuarioDel");

        vista.apuestaModButton.addActionListener(this);
        vista.apuestaModButton.setActionCommand("apuestaMod");
        vista.apuestaDelButton.addActionListener(this);
        vista.apuestaDelButton.setActionCommand("apuestaDel");

        vista.eventoAddButton.addActionListener(this);
        vista.eventoAddButton.setActionCommand("eventoAdd");
        vista.eventoModButton.addActionListener(this);
        vista.eventoModButton.setActionCommand("eventoMod");
        vista.eventoDelButton.addActionListener(this);
        vista.eventoDelButton.setActionCommand("eventoDel");

        vista.participanteAddButton.addActionListener(this);
        vista.participanteAddButton.setActionCommand("participanteAdd");
        vista.participanteModButton.addActionListener(this);
        vista.participanteModButton.setActionCommand("participanteMod");
        vista.participanteDelButton.addActionListener(this);
        vista.participanteDelButton.setActionCommand("participanteDel");
        vista.participanteCargarButton.addActionListener(this);
        vista.participanteCargarButton.setActionCommand("participanteImage");

        vista.campeonatoAddButton.addActionListener(this);
        vista.campeonatoAddButton.setActionCommand("campeonatoAdd");
        vista.campeonatoModButton.addActionListener(this);
        vista.campeonatoModButton.setActionCommand("campeonatoMod");
        vista.campeonatoDelButton.addActionListener(this);
        vista.campeonatoDelButton.setActionCommand("campeonatoDel");
        vista.campeonatoCargarButton.addActionListener(this);
        vista.campeonatoCargarButton.setActionCommand("campeonatoImage");

        vista.addparticipantesInsertarButton.addActionListener(this);
        vista.addparticipantesInsertarButton.setActionCommand("addparticipanteAdd");
        vista.addparticipantesRetirarButton.addActionListener(this);
        vista.addparticipantesRetirarButton.setActionCommand("addparticipanteDel");

        vista.deporteAddButton.addActionListener(this);
        vista.deporteAddButton.setActionCommand("deporteAdd");
        vista.deporteModButton.addActionListener(this);
        vista.deporteModButton.setActionCommand("deporteMod");
        vista.deporteDelButton.addActionListener(this);
        vista.deporteDelButton.setActionCommand("deporteDel");

        vista.rolAddButton.addActionListener(this);
        vista.rolAddButton.setActionCommand("rolAdd");
        vista.rolModButton.addActionListener(this);
        vista.rolModButton.setActionCommand("rolMod");
        vista.rolDelButton.addActionListener(this);
        vista.rolDelButton.setActionCommand("rolDel");

        vista.panelApuestaLocalButton.addActionListener(this);
        vista.panelApuestaLocalButton.setActionCommand("crearApuestaLocal");
        vista.panelApuestaVisitanteButton.addActionListener(this);
        vista.panelApuestaVisitanteButton.setActionCommand("crearApuestaVisitante");
        vista.panelApuestaButton.addActionListener(this);
        vista.panelApuestaButton.setActionCommand("crearApuestaCarrera");
        vista.cerrarPanel2.addActionListener(this);
        vista.cerrarPanel2.setActionCommand("cerrarPanel2");


        vista.marcarResultado2EquiposButton.addActionListener(this);
        vista.marcarResultado2EquiposButton.setActionCommand("marcarResultado2Equipos");
        vista.marcarResultadoCarreraButton.addActionListener(this);
        vista.marcarResultadoCarreraButton.setActionCommand("marcarResultadoCarrera");

        vista.miPerfilButton.addActionListener(this);
        vista.miPerfilButton.setActionCommand("perfil");

        vista.ayudaButton.addActionListener(this);
        vista.ayudaButton.setActionCommand("ayuda");

    }

    /**
     * Agrega los listeners de los combobox.
     *
     * @param controlador
     */
    private void addItemListeners(Controlador controlador) {
        vista.addparticipantesComboDeporte.addItemListener(controlador);
        vista.addparticipantesComboCompeticion.addItemListener(controlador);
        vista.eventoComboDeporte.addItemListener(controlador);
        vista.eventoComboCompeticion.addItemListener(controlador);
        vista.eventoComboLocal.addItemListener(controlador);
        vista.eventoComboVisitante.addItemListener(controlador);
    }

    /**
     * Agrega los listeners de las tablas y listas
     *
     * @param controlador
     */
    private void addListSelectionListeners(Controlador controlador) {
        vista.usuariosTabla.getSelectionModel().addListSelectionListener(this);
        vista.apuestasTabla.getSelectionModel().addListSelectionListener(this);
        vista.eventosTabla.getSelectionModel().addListSelectionListener(this);
        vista.participantesTabla.getSelectionModel().addListSelectionListener(this);
        vista.campeonatosTabla.getSelectionModel().addListSelectionListener(this);
        vista.deportesTabla.getSelectionModel().addListSelectionListener(this);
        vista.rolesTabla.getSelectionModel().addListSelectionListener(this);
        vista.tablaResultados.getSelectionModel().addListSelectionListener(this);

        vista.apuestaListaParticipantes.addListSelectionListener(controlador);
        vista.apuestaListaUsuarios.addListSelectionListener(controlador);
        vista.addparticipantesList1.addListSelectionListener(controlador);
        vista.addparticipantesList2.addListSelectionListener(controlador);
        vista.listaPilotosCarrera.addListSelectionListener(controlador);


    }

    /**
     * Agrega los listeners de objetos como imagenes
     *
     * @param controlador
     */
    private void addMouseListeners(Controlador controlador) {
        vista.image1.addMouseListener(controlador);
        vista.image2.addMouseListener(controlador);
        vista.image3.addMouseListener(controlador);
        vista.image4.addMouseListener(controlador);

    }

    /**
     * Agrega los listeners de cambio de pestañas.
     *
     * @param controlador
     */
    private void addChangeListeners(Controlador controlador) {
        vista.tabbedPanelPrincipal.addChangeListener(controlador);
        vista.tabbedPanelBBDD.addChangeListener(controlador);
        vista.eventoSliderPartido.addChangeListener(this);

    }

    /**
     * Agrega los listeners del calendario
     *
     * @param controlador
     */
    private void addPropertyChangeListeners(Controlador controlador) {
        vista.calendario.addPropertyChangeListener(controlador);
    }

    /**
     * Agrega los listeners de los campos de busqueda.
     *
     * @param controlador
     */
    private void addDocumentListeners(Controlador controlador) {

        vista.usuarioBusquedaTXT.getDocument().addDocumentListener(controlador);
        vista.apuestaBusquedaTXT.getDocument().addDocumentListener(controlador);
        vista.campeonatoBusquedaTXT.getDocument().addDocumentListener(controlador);
        vista.deporteBusquedaTXT.getDocument().addDocumentListener(controlador);
        vista.eventoBusquedaTXT.getDocument().addDocumentListener(controlador);
        vista.participanteBusquedaTXT.getDocument().addDocumentListener(controlador);
        vista.rolBusquedaTXT.getDocument().addDocumentListener(controlador);

    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            if (e.getSource().equals(vista.usuariosTabla.getSelectionModel()) && vista.usuariosTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.usuariosTabla.getSelectedRow();
                vista.usuarioNombreTXT.setText((String) vista.usuariosTabla.getValueAt(filaSeleccionada, 1));
                vista.usuarioApellidoTXT.setText((String) vista.usuariosTabla.getValueAt(filaSeleccionada, 2));
                vista.usuarioNickTXT.setText((String) vista.usuariosTabla.getValueAt(filaSeleccionada, 3));
                vista.usuarioPassTXT.setText((String) vista.usuariosTabla.getValueAt(filaSeleccionada, 4));
                vista.usuarioCorreoTXT.setText((String) vista.usuariosTabla.getValueAt(filaSeleccionada, 5));
                vista.usuarioPuntosTXT.setText(String.valueOf(vista.usuariosTabla.getValueAt(filaSeleccionada, 6)));
                vista.usuarioComboRol.setSelectedItem(vista.usuariosTabla.getValueAt(filaSeleccionada, 7));
            }
            if (e.getSource().equals(vista.apuestasTabla.getSelectionModel()) && vista.apuestasTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.apuestasTabla.getSelectedRow();
                Evento evento = (Evento) vista.apuestasTabla.getValueAt(filaSeleccionada, 2);
                vista.apuestaEventoTXT.setText("Evento: " + evento);
                vista.apuestaCantidadTXT.setText(vista.apuestasTabla.getValueAt(filaSeleccionada, 4).toString());
                vista.apuestaCheckGanada.setSelected(Util.byteToCheck((Byte) vista.apuestasTabla.getValueAt(filaSeleccionada, 5)));
                vista.apuestaCheckTerminada.setSelected(Util.byteToCheck((Byte) vista.apuestasTabla.getValueAt(filaSeleccionada, 6)));

                List<Participante> listaParticipantes = controladorCliente.getParticipantesEvento(evento.getId());
                listarParticipantesEvento(listaParticipantes, (Participante) vista.dtmApuestas.getValueAt(filaSeleccionada, 3));
            }
            if (e.getSource().equals(vista.eventosTabla.getSelectionModel()) && vista.eventosTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.eventosTabla.getSelectedRow();
                vista.eventoComboDeporte.setSelectedItem(vista.eventosTabla.getValueAt(filaSeleccionada, 1));
                vista.eventoComboCompeticion.setSelectedItem(vista.eventosTabla.getValueAt(filaSeleccionada, 2));
                vista.eventoDatePicker.setDateTimePermissive((LocalDateTime) vista.eventosTabla.getValueAt(filaSeleccionada, 3));
                Deporte d = (Deporte) vista.eventoComboDeporte.getSelectedItem();
                if (d.getNumParticipantes() == 2) {
                    List<Participanteevento> listaParticipantes = controladorCliente.getParticipanteseventoEvento((Integer) vista.eventosTabla.getValueAt(filaSeleccionada, 0));
                    vista.eventoComboLocal.setSelectedItem(listaParticipantes.get(0).getParticipante());
                    vista.eventoComboVisitante.setSelectedItem(listaParticipantes.get(1).getParticipante());
                    vista.eventoSliderPartido.setValue(Util.cuotaToPorcentaje(listaParticipantes.get(0).getCuota()));
                } else {
                    List<Participante> listaParticipantes = controladorCliente.getParticipantesEvento((Integer) vista.eventosTabla.getValueAt(filaSeleccionada, 0));
                    int[] indices = new int[listaParticipantes.size()];
                    for (int i = 0; i < vista.dlmparticipantesEvento.getSize(); i++) {
                        for (Participante participante : listaParticipantes) {
                            if (participante.getId() == vista.dlmparticipantesEvento.getElementAt(i).getId()) {
                                indices[listaParticipantes.indexOf(participante)] = i;
                            }
                        }
                    }
                    vista.eventoListaParticipantes.setSelectedIndices(indices);
                }
            }
            if (e.getSource().equals(vista.participantesTabla.getSelectionModel()) && vista.participantesTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.participantesTabla.getSelectedRow();
                vista.participanteComboDeporte.setSelectedItem(vista.participantesTabla.getValueAt(filaSeleccionada, 1));
                vista.participanteNombreTXT.setText((String) vista.participantesTabla.getValueAt(filaSeleccionada, 2));
                vista.participanteVictoriasTXT.setText(String.valueOf(vista.participantesTabla.getValueAt(filaSeleccionada, 3)));
                vista.participanteDerrotasTXT.setText(String.valueOf(vista.participantesTabla.getValueAt(filaSeleccionada, 4)));
                vista.participanteEmpatesTXT.setText(String.valueOf(vista.participantesTabla.getValueAt(filaSeleccionada, 5)));
                vista.participanteImageTXT.setText((String) vista.participantesTabla.getValueAt(filaSeleccionada, 6));
                setImageParticipante();
            }
            if (e.getSource().equals(vista.campeonatosTabla.getSelectionModel()) && vista.campeonatosTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.campeonatosTabla.getSelectedRow();
                vista.campeonatoComboDeporte.setSelectedItem(vista.campeonatosTabla.getValueAt(filaSeleccionada, 1));
                vista.campeonatoNombreTXT.setText((String) vista.campeonatosTabla.getValueAt(filaSeleccionada, 2));
                vista.campeonatoComboAnno.setSelectedItem(vista.campeonatosTabla.getValueAt(filaSeleccionada, 3));
                vista.campeonatoImageTXT.setText((String) vista.campeonatosTabla.getValueAt(filaSeleccionada, 4));
                setImageCampeonato();
            }
            if (e.getSource().equals(vista.deportesTabla.getSelectionModel()) && vista.deportesTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.deportesTabla.getSelectedRow();
                vista.deporteNombreTXT.setText((String) vista.deportesTabla.getValueAt(filaSeleccionada, 1));
                vista.deporteParticipantesTXT.setText((String.valueOf(vista.deportesTabla.getValueAt(filaSeleccionada, 2))));
            }
            if (e.getSource().equals(vista.rolesTabla.getSelectionModel()) && vista.rolesTabla.getSelectedRow() != -1) {
                int filaSeleccionada = vista.rolesTabla.getSelectedRow();
                vista.rolNombreTXT.setText((String) vista.rolesTabla.getValueAt(filaSeleccionada, 1));
                vista.rolCheckApostar.setSelected(Util.byteToCheck((Byte) vista.rolesTabla.getValueAt(filaSeleccionada, 2)));
                vista.rolCheckResultados.setSelected(Util.byteToCheck((Byte) vista.rolesTabla.getValueAt(filaSeleccionada, 3)));
                vista.rolCheckPartidos.setSelected(Util.byteToCheck((Byte) vista.rolesTabla.getValueAt(filaSeleccionada, 4)));
                vista.rolCheckUsuarios.setSelected(Util.byteToCheck((Byte) vista.rolesTabla.getValueAt(filaSeleccionada, 5)));
            }
            if (e.getSource().equals(vista.listaPilotosCarrera) && vista.listaPilotosCarrera.getSelectedIndex() != -1) {
                Participanteevento p = vista.dlmparticipantesCarreraApuesta.getElementAt(vista.listaPilotosCarrera.getSelectedIndex());
                setImagePiloto(p.getParticipante().getLogo());
                vista.apostarPilotoCarreraTXT.setText(p.getParticipante().getNombre());
                vista.apostarCuotaCarreraTXT.setText("Cuota: \n" + p.getCuota());
            }

            if (e.getSource().equals(vista.apuestaListaUsuarios) && vista.apuestaListaUsuarios.getSelectedIndex() != -1) {
                listarApuestas((Usuario) vista.apuestaListaUsuarios.getSelectedValue());
            }


            if (e.getSource().equals(vista.tablaResultados.getSelectionModel()) && vista.tablaResultados.getSelectedRow() != -1) {
                int filaSeleccionada = vista.tablaResultados.getSelectedRow();
                vista.panel2Participantes.setVisible(false);
                vista.panelMasParticipantes.setVisible(false);
                Deporte d = (Deporte) vista.tablaResultados.getValueAt(filaSeleccionada, 1);
                List<Participanteevento> listaParticipantes = controladorCliente.getParticipanteseventoEvento((Integer) vista.tablaResultados.getValueAt(filaSeleccionada, 0));
                if (d.getNumParticipantes() == 2) {
                    vista.panel2Participantes.setVisible(true);
                    vista.resultadoEquipo1.setText(listaParticipantes.get(0).getParticipante().getNombre());
                    vista.resultadoEquipo2.setText(listaParticipantes.get(1).getParticipante().getNombre());
                } else {
                    vista.panelMasParticipantes.setVisible(true);
                    for (Participanteevento participante : listaParticipantes) {
                        vista.dlmGanador.addElement(participante);
                    }
                }
            }
        }


    }




    /**
     * Metodo que asigna las acciones a los botones
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String command = e.getActionCommand();

        switch (command) {
            case "usuarioAdd":
                addUsuario();
                break;
            case "usuarioMod":
                modUsuario();
                break;
            case "usuarioDel":
                delUsuario();
                break;

            case "apuestaMod":
                modApuesta();
                break;
            case "apuestaDel":
                delApuesta();
                break;

            case "eventoAdd":
                addEvento();
                break;
            case "eventoMod":
                modEvento();
                break;
            case "eventoDel":
                delEvento();
                break;

            case "participanteAdd":
                addParticipante();
                break;
            case "participanteMod":
                modParticipante();
                break;
            case "participanteDel":
                delParticipante();
                break;
            case "participanteImage":
                setImageParticipante();
                break;

            case "campeonatoAdd":
                addCampeonato();
                break;
            case "campeonatoMod":
                modCampeonato();
                break;
            case "campeonatoDel":
                delCampeonato();
                break;
            case "campeonatoImage":
                setImageCampeonato();
                break;

            case "addparticipanteAdd":
                addAddParticipante();
                break;
            case "addparticipanteDel":
                delAddParticipante();
                break;

            case "deporteAdd":
                addDeporte();
                break;
            case "deporteMod":
                modDeporte();
                break;
            case "deporteDel":
                delDeporte();
                break;

            case "rolAdd":
                addRol();
                break;
            case "rolMod":
                modRol();
                break;
            case "rolDel":
                delRol();
                break;

            case "crearApuestaLocal":
                setApuesta("local");
                refrescarUsuario();
                crearApuestasActuales();
                break;
            case "crearApuestaVisitante":
                setApuesta("visitante");
                refrescarUsuario();
                crearApuestasActuales();
                break;
            case "crearApuestaCarrera":
                setApuesta("carrera");
                refrescarUsuario();
                crearApuestasActuales();
                break;
            case "cerrarPanel2":
                vista.panelApuestaPartido.setVisible(false);
                break;

            case "marcarResultado2Equipos":
                setResultado("partido");
                break;
            case "marcarResultadoCarrera":
                setResultado("carrera");
                break;

            case "perfil":
                setVistaPerfil();
                break;

            case "ayuda":
                ayuda();
                break;

        }

    }



    /**
     * Metodo que crea la vista de un perfil
     */
    private void setVistaPerfil() {
        usuario = controladorCliente.sessionUser;
        vista.tabbedPanelPrincipal.insertTab("Perfil", null, vista.panelPerfil, null, vista.tabbedPanelPrincipal.getTabCount());
        vista.tabbedPanelPrincipal.setSelectedIndex(vista.tabbedPanelPrincipal.getTabCount() - 1);
        vista.perfilUsername.setText(usuario.getUsername());
        vista.perfilNombre.setText(usuario.getNombre());
        vista.perfilApellidos.setText(usuario.getApellidos());
        vista.perfilEmail.setText(usuario.getEmail());
        vista.perfilPuntos.setText(usuario.getPuntos() + "");
        crearApuestasPerfil();
        crearRanking();

    }


    /**
     * Se ejecuta cada vez que selecciono algo en un comboBox
     * Lo que hace es listar los jugadores de esos equipos
     *
     * @param e
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == vista.addparticipantesComboDeporte && e.getStateChange() == ItemEvent.SELECTED) {
            listarCampeonatosDeporte(vista.addparticipantesComboDeporte.getSelectedItem());
            listarParticipantesDeporte(vista.addparticipantesComboDeporte.getSelectedItem());
            listarParticipantesCampeonato(vista.addparticipantesComboCompeticion.getSelectedItem());
        }
        if (e.getSource() == vista.addparticipantesComboCompeticion && e.getStateChange() == ItemEvent.SELECTED) {
            listarParticipantesCampeonato(vista.addparticipantesComboCompeticion.getSelectedItem());
        }
        if (e.getSource() == vista.eventoComboDeporte && e.getStateChange() == ItemEvent.SELECTED) {
            if (((Deporte) vista.eventoComboDeporte.getSelectedItem()).getNumParticipantes() == 2) {
                vista.eventoPanel2Equipos.setVisible(true);
                vista.eventoPanelMasParticipantes.setVisible(false);
            } else {
                vista.eventoPanel2Equipos.setVisible(false);
                vista.eventoPanelMasParticipantes.setVisible(true);
            }
            listarCampeonatosDeporteEvento(vista.eventoComboDeporte.getSelectedItem());
            listarParticipantesCampeonatoEvento(vista.eventoComboCompeticion.getSelectedItem());
        }
        if (e.getSource() == vista.eventoComboCompeticion && e.getStateChange() == ItemEvent.SELECTED) {
            listarParticipantesCampeonatoEvento(vista.eventoComboCompeticion.getSelectedItem());
        }

        if (e.getSource() == vista.eventoComboLocal || e.getSource() == vista.eventoComboVisitante && e.getStateChange() == ItemEvent.SELECTED) {
            calcularCuotaAutomatica();
        }

    }

    /**
     * Metodo que añade un usuario.
     */
    private void addUsuario() {
        try {
            if (comprobarUsuarioVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.usuariosTabla.clearSelection();
            } else {
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setNombre(vista.usuarioNombreTXT.getText());
                nuevoUsuario.setApellidos(vista.usuarioApellidoTXT.getText());
                nuevoUsuario.setUsername(vista.usuarioNickTXT.getText());
                nuevoUsuario.setPass(vista.usuarioPassTXT.getText());
                nuevoUsuario.setEmail(vista.usuarioCorreoTXT.getText());
                nuevoUsuario.setPuntos(Integer.parseInt(vista.usuarioPuntosTXT.getText()));
                nuevoUsuario.setRol((Rol) vista.usuarioComboRol.getSelectedItem());
                controladorCliente.insertarUsuario(nuevoUsuario);

                vista.dtmUsuarios.addRow(Util.usuarioToRow(nuevoUsuario));
                borrarCamposUsuario();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.usuariosTabla.clearSelection();
        }
    }

    /**
     * Metodo que modifica un usuario.
     */
    private void modUsuario() {
        int filaSeleccionada = vista.usuariosTabla.getSelectedRow();

        try {
            if (comprobarUsuarioVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.usuariosTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona un usuario");
                vista.usuariosTabla.clearSelection();
            } else {
                int idUsuario = (int) vista.dtmUsuarios.getValueAt(filaSeleccionada, 0);
                Usuario usuarioModificado = controladorCliente.getUsuario(idUsuario);
                usuarioModificado.setNombre(vista.usuarioNombreTXT.getText());
                usuarioModificado.setApellidos(vista.usuarioApellidoTXT.getText());
                usuarioModificado.setUsername(vista.usuarioNickTXT.getText());
                usuarioModificado.setPass(vista.usuarioPassTXT.getText());
                usuarioModificado.setEmail(vista.usuarioCorreoTXT.getText());
                usuarioModificado.setPuntos(Integer.parseInt(vista.usuarioPuntosTXT.getText()));
                usuarioModificado.setRol((Rol) vista.usuarioComboRol.getSelectedItem());
                controladorCliente.insertarUsuario(usuarioModificado);

                vista.dtmUsuarios.removeRow(filaSeleccionada);
                vista.dtmUsuarios.insertRow(filaSeleccionada, Util.usuarioToRow(usuarioModificado));
                borrarCamposUsuario();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.usuariosTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.usuariosTabla.clearSelection();
        }
    }

    /**
     * Metodo que elimina un usuario.
     */
    private void delUsuario() {
        int filaSeleccionada = vista.usuariosTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idUsuario = (int) vista.dtmUsuarios.getValueAt(filaSeleccionada, 0);
            controladorCliente.borrarUsuario(controladorCliente.getUsuario(idUsuario));
            vista.dtmUsuarios.removeRow(filaSeleccionada);
            borrarCamposUsuario();
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que modifica una apuesta.
     */
    private void modApuesta() {
        int filaSeleccionada = vista.apuestasTabla.getSelectedRow();

        try {
            if (comprobarApuestaVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.apuestasTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona una apuesta");
                vista.apuestasTabla.clearSelection();
            } else {
                int idApuesta = (int) vista.dtmApuestas.getValueAt(filaSeleccionada, 0);
                Apuesta apuestaModificada = controladorCliente.getApuesta(idApuesta);
                apuestaModificada.setCantidad(Integer.parseInt(vista.apuestaCantidadTXT.getText()));
                apuestaModificada.setGanada(Util.checkToByte(vista.apuestaCheckGanada));
                apuestaModificada.setTerminada(Util.checkToByte(vista.apuestaCheckTerminada));
                apuestaModificada.setApuesta((Participante) vista.apuestaListaParticipantes.getSelectedValue());
                controladorCliente.modificarApuesta(apuestaModificada);

                vista.dtmApuestas.removeRow(filaSeleccionada);
                vista.dtmApuestas.insertRow(filaSeleccionada, Util.apuestaToRow(apuestaModificada));
                borrarCamposApuesta();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.apuestasTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.apuestasTabla.clearSelection();
        }

    }

    /**
     * Metodo que elimina una apuesta
     */
    private void delApuesta() {
        int filaSeleccionada = vista.apuestasTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idApuesta = (int) vista.dtmApuestas.getValueAt(filaSeleccionada, 0);
            controladorCliente.borrarApuesta(controladorCliente.getApuesta(idApuesta));
            vista.dtmApuestas.removeRow(filaSeleccionada);
            borrarCamposApuesta();
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que añade un evento
     */
    private void addEvento() {
        try {
            if (comprobarEventoVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.eventosTabla.clearSelection();
            } else {
                Evento nuevoEvento = new Evento();
                nuevoEvento.setCampeonato((Campeonato) vista.eventoComboCompeticion.getSelectedItem());
                nuevoEvento.setFecha(vista.eventoDatePicker.getDateTimePermissive());
                nuevoEvento.setTerminado(Util.checkToByte(vista.eventoCheckTerminado));
                Evento evento = controladorCliente.insertarEvento(nuevoEvento);

                if (((Deporte) vista.eventoComboDeporte.getSelectedItem()).getNumParticipantes() == 2) {
                    Participanteevento participanteevento = new Participanteevento();
                    participanteevento.setEvento(evento);
                    participanteevento.setParticipante((Participante) vista.eventoComboLocal.getSelectedItem());
                    participanteevento.setCuota(Util.textToDouble(vista.eventoCuotaLocal.getText()));
                    controladorCliente.insertarParticipanteEvento(participanteevento);

                    participanteevento = new Participanteevento();
                    participanteevento.setEvento(evento);
                    participanteevento.setParticipante((Participante) vista.eventoComboVisitante.getSelectedItem());
                    participanteevento.setCuota(Util.textToDouble(vista.eventoCuotaVisitante.getText()));
                    controladorCliente.insertarParticipanteEvento(participanteevento);

                } else {
                    int[] seleccion = vista.eventoListaParticipantes.getSelectedIndices();
                    for (int i = 0; i < seleccion.length; i++) {
                        int selectedRow = seleccion[i];
                        Participanteevento participanteevento = new Participanteevento();
                        participanteevento.setEvento(evento);
                        participanteevento.setParticipante(vista.dlmparticipantesEvento.getElementAt(selectedRow));
                        participanteevento.setCuota(calcularCuotaIndividual(vista.dlmparticipantesEvento.getElementAt(selectedRow)));
                        controladorCliente.insertarParticipanteEvento(participanteevento);
                    }
                }
                listarEventos();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.participantesTabla.clearSelection();
        }
    }

    /**
     * Metodo que modifica un evento
     */
    private void modEvento() {
        int filaSeleccionada = vista.eventosTabla.getSelectedRow();

        try {
            if (comprobarEventoVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.eventosTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona un participante");
                vista.eventosTabla.clearSelection();
            } else {
                int idEvento = (int) vista.dtmEventos.getValueAt(filaSeleccionada, 0);
                Evento eventoModificado = controladorCliente.getEvento(idEvento);
                eventoModificado.setCampeonato((Campeonato) vista.eventoComboCompeticion.getSelectedItem());
                eventoModificado.setFecha(vista.eventoDatePicker.getDateTimePermissive());
                eventoModificado.setTerminado(Util.checkToByte(vista.eventoCheckTerminado));
                controladorCliente.borrarParticipantesEvento(eventoModificado);
                Evento evento = controladorCliente.insertarEvento(eventoModificado);

                if (((Deporte) vista.eventoComboDeporte.getSelectedItem()).getNumParticipantes() == 2) {
                    Participanteevento participanteevento = new Participanteevento();
                    participanteevento.setEvento(evento);
                    participanteevento.setParticipante((Participante) vista.eventoComboLocal.getSelectedItem());
                    participanteevento.setCuota(Util.textToDouble(vista.eventoCuotaLocal.getText()));
                    controladorCliente.insertarParticipanteEvento(participanteevento);

                    participanteevento = new Participanteevento();
                    participanteevento.setEvento(evento);
                    participanteevento.setParticipante((Participante) vista.eventoComboVisitante.getSelectedItem());
                    participanteevento.setCuota(Util.textToDouble(vista.eventoCuotaVisitante.getText()));
                    controladorCliente.insertarParticipanteEvento(participanteevento);

                } else {
                    int[] seleccion = vista.eventoListaParticipantes.getSelectedIndices();
                    for (int i = 0; i < seleccion.length; i++) {
                        int selectedRow = seleccion[i];
                        Participanteevento participanteevento = new Participanteevento();
                        participanteevento.setEvento(evento);
                        participanteevento.setParticipante(vista.dlmparticipantesEvento.getElementAt(selectedRow));
                        participanteevento.setCuota(calcularCuotaIndividual(vista.dlmparticipantesEvento.getElementAt(selectedRow)));
                        controladorCliente.insertarParticipanteEvento(participanteevento);
                    }
                }
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.eventosTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.eventosTabla.clearSelection();
        }
    }

    /**
     * Metodo que elimina un evento
     */
    private void delEvento() {
        int filaSeleccionada = vista.eventosTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idEvento = (int) vista.dtmEventos.getValueAt(filaSeleccionada, 0);
            Evento e = controladorCliente.getEvento(idEvento);
            controladorCliente.borrarEvento(e);
            vista.dtmEventos.removeRow(filaSeleccionada);
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que añade un participante
     */
    private void addParticipante() {
        try {
            if (comprobarParticipanteVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.participantesTabla.clearSelection();
            } else {
                Participante nuevoParticipante = new Participante();
                nuevoParticipante.setDeporte((Deporte) vista.participanteComboDeporte.getSelectedItem());
                nuevoParticipante.setNombre(vista.participanteNombreTXT.getText());
                nuevoParticipante.setLogo(vista.participanteImageTXT.getText());
                nuevoParticipante.setVictorias(Integer.parseInt(vista.participanteVictoriasTXT.getText()));
                nuevoParticipante.setDerrotas(Integer.parseInt(vista.participanteDerrotasTXT.getText()));
                nuevoParticipante.setEmpates(Integer.parseInt(vista.participanteEmpatesTXT.getText()));
                controladorCliente.insertarParticipante(nuevoParticipante);

                vista.dtmParticipantes.addRow(Util.participanteToRow(nuevoParticipante));
                borrarCamposParticipante();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.participantesTabla.clearSelection();
        }
    }

    /**
     * Metodo que modifica un participante
     */
    private void modParticipante() {
        int filaSeleccionada = vista.participantesTabla.getSelectedRow();

        try {
            if (comprobarParticipanteVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.participantesTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona un participante");
                vista.participantesTabla.clearSelection();
            } else {
                int idParticipante = (int) vista.dtmParticipantes.getValueAt(filaSeleccionada, 0);
                Participante participanteModificado = controladorCliente.getParticipante(idParticipante);
                participanteModificado.setDeporte((Deporte) vista.participanteComboDeporte.getSelectedItem());
                participanteModificado.setNombre(vista.participanteNombreTXT.getText());
                participanteModificado.setLogo(vista.participanteImageTXT.getText());
                participanteModificado.setVictorias(Integer.parseInt(vista.participanteVictoriasTXT.getText()));
                participanteModificado.setDerrotas(Integer.parseInt(vista.participanteDerrotasTXT.getText()));
                participanteModificado.setEmpates(Integer.parseInt(vista.participanteEmpatesTXT.getText()));
                controladorCliente.insertarParticipante(participanteModificado);

                vista.dtmParticipantes.removeRow(filaSeleccionada);
                vista.dtmParticipantes.insertRow(filaSeleccionada, Util.participanteToRow(participanteModificado));
                borrarCamposParticipante();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.participantesTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.participantesTabla.clearSelection();
        }
    }

    /**
     * Metodo que borra un participante
     */
    private void delParticipante() {
        int filaSeleccionada = vista.participantesTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idParticipante = (int) vista.dtmParticipantes.getValueAt(filaSeleccionada, 0);
            controladorCliente.borrarParticipante(controladorCliente.getParticipante(idParticipante));
            vista.dtmParticipantes.removeRow(filaSeleccionada);
            borrarCamposParticipante();
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Crea la lista de participantes de un evento.
     *
     * @param listaParticipantes
     * @param participante
     */
    private void listarParticipantesEvento(List<Participante> listaParticipantes, Participante participante) {

        vista.dlmparticipantesApuesta.clear();
        for (int i = 0; i < listaParticipantes.size(); i++) {
            vista.dlmparticipantesApuesta.addElement(listaParticipantes.get(i));
            if (vista.dlmparticipantesApuesta.elementAt(i) == participante) {
                vista.apuestaListaParticipantes.setSelectedIndex(i);
            }
        }
    }

    /**
     * Metodo que añade un campeonato
     */
    private void addCampeonato() {
        try {
            if (comprobarCampeonatoVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.campeonatosTabla.clearSelection();
            } else {
                Campeonato nuevoCampeonato = new Campeonato();
                nuevoCampeonato.setDeporte((Deporte) vista.campeonatoComboDeporte.getSelectedItem());
                nuevoCampeonato.setAnno(Integer.parseInt(vista.campeonatoComboAnno.getSelectedItem().toString()));
                nuevoCampeonato.setNombre(vista.campeonatoNombreTXT.getText());
                nuevoCampeonato.setLogo(vista.campeonatoImageTXT.getText());
                controladorCliente.insertarCampeonato(nuevoCampeonato);
                vista.dtmCampeonato.addRow(Util.campeonatoToRow(nuevoCampeonato));
                borrarCamposDeporte();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.campeonatosTabla.clearSelection();
        }
    }

    /**
     * Metodo que modifica un campeonato
     */
    private void modCampeonato() {
        int filaSeleccionada = vista.campeonatosTabla.getSelectedRow();

        try {
            if (comprobarCampeonatoVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.campeonatosTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona un campeonato");
                vista.campeonatosTabla.clearSelection();
            } else {
                int idCampeonato = (int) vista.dtmCampeonato.getValueAt(filaSeleccionada, 0);
                Campeonato campeonatoModificado = controladorCliente.getCampeonato(idCampeonato);
                campeonatoModificado.setDeporte((Deporte) vista.campeonatoComboDeporte.getSelectedItem());
                campeonatoModificado.setAnno(Integer.parseInt(vista.campeonatoComboAnno.getSelectedItem().toString()));
                campeonatoModificado.setNombre(vista.campeonatoNombreTXT.getText());
                campeonatoModificado.setLogo(vista.campeonatoImageTXT.getText());
                controladorCliente.insertarCampeonato(campeonatoModificado);

                vista.dtmCampeonato.removeRow(filaSeleccionada);
                vista.dtmCampeonato.insertRow(filaSeleccionada, Util.campeonatoToRow(campeonatoModificado));
                borrarCamposCampeonato();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.campeonatosTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.campeonatosTabla.clearSelection();
        }
    }

    /**
     * Metodo que borra un campeonato
     */
    private void delCampeonato() {
        int filaSeleccionada = vista.campeonatosTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idCampeonato = (int) vista.dtmCampeonato.getValueAt(filaSeleccionada, 0);
            controladorCliente.borrarCampeonato(controladorCliente.getCampeonato(idCampeonato));
            vista.dtmCampeonato.removeRow(filaSeleccionada);
            borrarCamposCampeonato();
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que añade un participante a un campeonato
     */
    private void addAddParticipante() {
        try {
            if (vista.addparticipantesList1.getSelectedIndex() == -1) {
                Util.showErrorAlert("Rellena todos los campos");
            } else {
                int[] seleccion = vista.addparticipantesList1.getSelectedIndices();
                for (int i = 0; i < seleccion.length; i++) {
                    int selectedRow = seleccion[i];
                    Participante p = vista.dlmparticipantesDeporte.getElementAt(selectedRow);
                    if (comprobarParticipanteCompeticionExiste(p)) {
                        Util.showInfoAlert("El participante " + p + " ya esta");
                    } else {
                        Participantecampeonato nuevoParticipante = new Participantecampeonato();
                        nuevoParticipante.setCampeonato((Campeonato) vista.addparticipantesComboCompeticion.getSelectedItem());
                        nuevoParticipante.setParticipante(vista.dlmparticipantesDeporte.getElementAt(selectedRow));
                        controladorCliente.insertarParticipanteCampeonato(nuevoParticipante);
                    }
                }

            }
        } catch (Exception e) {
        }
        listarParticipantesCampeonato(vista.addparticipantesComboCompeticion.getSelectedItem());

    }


    /**
     * Metodo que borra un participante a un campeonato
     */
    private void delAddParticipante() {
        int filaSeleccionada = vista.addparticipantesList2.getSelectedIndex();
        if (filaSeleccionada != -1) {
            int[] seleccion = vista.addparticipantesList2.getSelectedIndices();
            for (int i = 0; i < seleccion.length; i++) {
                int selectedRow = seleccion[i];
                Participantecampeonato participantecampeonato = vista.dlmparticipantesCompeticion.elementAt(selectedRow);
                controladorCliente.borrarParticipantecampeonato(participantecampeonato);
            }
            listarParticipantesCampeonato(vista.addparticipantesComboCompeticion.getSelectedItem());
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que añade un deporte
     */
    private void addDeporte() {
        try {
            if (comprobarDeporteVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.deportesTabla.clearSelection();
            } else {
                Deporte nuevoDeporte = new Deporte();
                nuevoDeporte.setNombre(vista.deporteNombreTXT.getText());
                nuevoDeporte.setNumParticipantes(Integer.parseInt(vista.deporteParticipantesTXT.getText()));
                controladorCliente.insertarDeporte(nuevoDeporte);

                vista.dtmDeportes.addRow(Util.deporteToRow(nuevoDeporte));
                borrarCamposDeporte();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.deportesTabla.clearSelection();
        }
    }

    /**
     * Metodo que modifica un deporte
     */
    private void modDeporte() {
        int filaSeleccionada = vista.deportesTabla.getSelectedRow();

        try {
            if (comprobarDeporteVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.deportesTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona un deporte");
                vista.deportesTabla.clearSelection();
            } else {
                int idDeportes = (int) vista.dtmDeportes.getValueAt(filaSeleccionada, 0);
                Deporte deporteModificado = controladorCliente.getDeporte(idDeportes);
                deporteModificado.setNombre(vista.deporteNombreTXT.getText());
                deporteModificado.setNumParticipantes(Integer.parseInt(vista.deporteParticipantesTXT.getText()));
                controladorCliente.insertarDeporte(deporteModificado);

                vista.dtmDeportes.removeRow(filaSeleccionada);
                vista.dtmDeportes.insertRow(filaSeleccionada, Util.deporteToRow(deporteModificado));
                borrarCamposDeporte();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.deportesTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.deportesTabla.clearSelection();
        }
    }

    /**
     * Metodo que elimina un deporte
     */
    private void delDeporte() {
        int filaSeleccionada = vista.deportesTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idDeporte = (int) vista.dtmDeportes.getValueAt(filaSeleccionada, 0);
            controladorCliente.borrarDeporte(controladorCliente.getDeporte(idDeporte));
            vista.dtmDeportes.removeRow(filaSeleccionada);
            borrarCamposDeporte();
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que añade un rol.
     */
    private void addRol() {
        try {
            if (comprobarRolVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.usuariosTabla.clearSelection();
            } else {
                Rol nuevoRol = new Rol();
                nuevoRol.setDescripcion(vista.rolNombreTXT.getText());
                nuevoRol.setPermisoApuesta(Util.checkToByte(vista.rolCheckApostar));
                nuevoRol.setPermisoModificarResultados(Util.checkToByte(vista.rolCheckResultados));
                nuevoRol.setPermisoModificarPartidos(Util.checkToByte(vista.rolCheckPartidos));
                nuevoRol.setPermisoModificarUsuarios(Util.checkToByte(vista.rolCheckUsuarios));
                controladorCliente.insertarRol(nuevoRol);

                vista.dtmRoles.addRow(Util.rolToRow(nuevoRol));
                borrarCamposRol();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.rolesTabla.clearSelection();
        }
    }

    /**
     * Metodo que modifica un rol
     */
    private void modRol() {
        int filaSeleccionada = vista.rolesTabla.getSelectedRow();

        try {
            if (comprobarRolVacio()) {
                Util.showErrorAlert("Rellena todos los campos");
                vista.rolesTabla.clearSelection();
            }
            if (filaSeleccionada == -1) {
                Util.showErrorAlert("Selecciona un rol");
                vista.rolesTabla.clearSelection();
            } else {
                int idRol = (int) vista.dtmRoles.getValueAt(filaSeleccionada, 0);
                Rol rolModificado = controladorCliente.getRol(idRol);
                rolModificado.setDescripcion(vista.rolNombreTXT.getText());
                rolModificado.setPermisoApuesta(Util.checkToByte(vista.rolCheckApostar));
                rolModificado.setPermisoModificarResultados(Util.checkToByte(vista.rolCheckResultados));
                rolModificado.setPermisoModificarPartidos(Util.checkToByte(vista.rolCheckPartidos));
                rolModificado.setPermisoModificarUsuarios(Util.checkToByte(vista.rolCheckUsuarios));
                controladorCliente.insertarRol(rolModificado);

                vista.dtmRoles.removeRow(filaSeleccionada);
                vista.dtmRoles.insertRow(filaSeleccionada, Util.rolToRow(rolModificado));
                borrarCamposRol();
            }
        } catch (NumberFormatException nfe) {
            Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
            vista.rolesTabla.clearSelection();
        } catch (NullPointerException npe) {
            Util.showErrorAlert("Selecciona un dato");
            vista.rolesTabla.clearSelection();
        }
    }

    /**
     * Metodo que elimina un rol
     */
    private void delRol() {
        int filaSeleccionada = vista.rolesTabla.getSelectedRow();
        if (filaSeleccionada != -1) {
            int idRol = (int) vista.dtmRoles.getValueAt(filaSeleccionada, 0);
            controladorCliente.borrarRol(controladorCliente.getRol(idRol));
            vista.dtmRoles.removeRow(filaSeleccionada);
            borrarCamposRol();
        } else {
            Util.showErrorAlert("Selecciona un dato");
        }
    }

    /**
     * Metodo que comprueba si algun campo de un usuario esta vacio.
     *
     * @return
     */
    private boolean comprobarUsuarioVacio() {
        return vista.usuarioNombreTXT.getText().isEmpty() ||
                vista.usuarioApellidoTXT.getText().isEmpty() ||
                vista.usuarioNickTXT.getText().isEmpty() ||
                vista.usuarioPassTXT.getText().isEmpty() ||
                vista.usuarioCorreoTXT.getText().isEmpty() ||
                vista.usuarioPuntosTXT.getText().isEmpty() ||
                vista.usuarioComboRol.getSelectedIndex() == -1;
    }

    /**
     * Metodo que borra los campos de usuario
     */
    private void borrarCamposUsuario() {
        vista.usuarioNombreTXT.setText("");
        vista.usuarioApellidoTXT.setText("");
        vista.usuarioNickTXT.setText("");
        vista.usuarioPassTXT.setText("");
        vista.usuarioCorreoTXT.setText("");
        vista.usuarioPuntosTXT.setText("");
    }

    /**
     * Metodo que comprueba que una apuesta esta vacia
     *
     * @return
     */
    private boolean comprobarApuestaVacio() {
        return vista.apuestaCantidadTXT.getText().isEmpty() ||
                vista.apuestaListaParticipantes.getSelectedIndex() == -1;
    }

    /**
     * Metodo que borra los campos de apuesta.
     */
    private void borrarCamposApuesta() {
        vista.apuestaCantidadTXT.setText("");
        vista.apuestaCheckGanada.setSelected(false);
        vista.apuestaCheckTerminada.setSelected(false);
        vista.apuestaListaParticipantes.setSelectedIndex(-1);
    }

    /**
     * Metodo que comprueba que algun campo de evento esta vacio
     *
     * @return
     */
    private boolean comprobarEventoVacio() {
        return vista.eventoComboDeporte.getSelectedIndex() == -1 ||
                vista.eventoDatePicker.getDateTimePermissive() == null ||
                vista.eventoComboCompeticion.getSelectedIndex() == -1;
    }

    /**
     * Metodo que borra los campos de evento
     */
    private void borrarCamposEvento() {
        vista.eventoComboDeporte.setSelectedIndex(-1);
        vista.eventoDatePicker.clear();
        vista.eventoCheckTerminado.setSelected(false);
        vista.eventoComboCompeticion.setSelectedIndex(-1);
        vista.eventoComboLocal.setSelectedItem(-1);
        vista.eventoComboVisitante.setSelectedItem(-1);
        vista.eventoSliderPartido.setValue(50);
    }

    /**
     * Metodo que comprueba que algun campo de participante esta vacio
     *
     * @return
     */
    private boolean comprobarParticipanteVacio() {
        return vista.participanteComboDeporte.getSelectedIndex() == -1 ||
                vista.participanteNombreTXT.getText().isEmpty() ||
                vista.participanteImageTXT.getText().isEmpty() ||
                vista.participanteVictoriasTXT.getText().isEmpty() ||
                vista.participanteDerrotasTXT.getText().isEmpty() ||
                vista.participanteEmpatesTXT.getText().isEmpty();
    }

    /**
     * Metodo que borra los campos de participante
     */
    private void borrarCamposParticipante() {
        vista.participanteComboDeporte.setSelectedIndex(-1);
        vista.participanteNombreTXT.setText("");
        vista.participanteImageTXT.setText("");
        vista.participanteVictoriasTXT.setText("");
        vista.participanteDerrotasTXT.setText("");
        vista.participanteEmpatesTXT.setText("");
    }

    /**
     * Metodo que comprueba que algun campo de campeonato esta vacio.
     *
     * @return
     */
    private boolean comprobarCampeonatoVacio() {
        return vista.campeonatoComboDeporte.getSelectedIndex() == -1 ||
                vista.campeonatoComboAnno.getSelectedIndex() == -1 ||
                vista.campeonatoNombreTXT.getText().isEmpty() ||
                vista.campeonatoImageTXT.getText().isEmpty();
    }

    /**
     * Metodo que borra los campos de campeonato.
     */
    private void borrarCamposCampeonato() {
        vista.campeonatoComboDeporte.setSelectedIndex(-1);
        vista.campeonatoComboAnno.setSelectedIndex(-1);
        vista.campeonatoNombreTXT.setText("");
        vista.campeonatoImageTXT.setText("");
    }

    /**
     * Metodo que comprueba que algun campo de deporte esta vacio.
     *
     * @return
     */
    private boolean comprobarDeporteVacio() {
        return vista.deporteNombreTXT.getText().isEmpty() ||
                vista.deporteParticipantesTXT.getText().isEmpty();
    }

    /**
     * Metodo que borra los campos de deporte
     */
    private void borrarCamposDeporte() {
        vista.deporteNombreTXT.setText("");
        vista.deporteParticipantesTXT.setText("");
    }

    /**
     * Metodo que comprueba que un campo rol esta vacio.
     *
     * @return
     */
    private boolean comprobarRolVacio() {
        return vista.rolNombreTXT.getText().isEmpty();
    }

    /**
     * Metodo que comprueba que un campo rol esta vacio.
     */
    private void borrarCamposRol() {
        vista.rolNombreTXT.setText("");
        vista.rolCheckApostar.setSelected(false);
        vista.rolCheckResultados.setSelected(false);
        vista.rolCheckPartidos.setSelected(false);
        vista.rolCheckUsuarios.setSelected(false);
    }

    /**
     * Metodo que comprueba si algun campo de apuesta esta vacio
     *
     * @return
     */
    private boolean comprobarCrearApuestaVacia() {
        return vista.panelApuestaPuntosTXT.getText().isEmpty();
    }

    /**
     * Metodo que comprueba si algun campo de apuesta de carrera esta vacio.
     *
     * @return
     */
    private boolean comprobarCrearApuestaCarreraVacia() {
        return vista.listaPilotosCarrera.getSelectedIndex() == -1 ||
                vista.panelApuestaPuntosCarreraTXT.getText().isEmpty();
    }


    /**
     * Metodo que se ejecuta cada vez que se cambia de pestaña
     *
     * @param e
     */
    @Override
    public void stateChanged(ChangeEvent e) {


        if (e.getSource() == vista.eventoSliderPartido && vista.eventoSliderPartido.isEnabled()) {
            calcularCuotaManual();
        } else {
            if (vista.tabbedPanelPrincipal.getSelectedIndex() == 2) {
                int panelSeleccionado = vista.tabbedPanelBBDD.getSelectedIndex();
                switch (panelSeleccionado) {
                    case 0:
                        listarRolesCB();
                        listarUsuarios();
                        break;
                    case 1:
                        listarUsuariosCB();
                        break;
                    case 2:
                        listarEventos();
                        listarDeportesCB();
                        break;
                    case 3:
                        listarParticipantes();
                        listarDeportesCB();
                        break;
                    case 4:
                        listarCampeonatos();
                        listarDeportesCB();
                        break;
                    case 5:
                        listarDeportesCB();
                        break;
                    case 6:
                        listarRoles();
                        break;
                    case 7:
                        listarDeportes();
                        break;
                }

            }
            if (vista.tabbedPanelPrincipal.getSelectedIndex() == 1) {
                listarEventosSinResultado();
            }
            if (vista.tabbedPanelPrincipal.getSelectedIndex() != 3) {

            }

        }


    }

    /**
     * Metodo que crea una lista de deportes para un cb
     */
    private void listarDeportesCB() {
        List<Deporte> listaDeportes = controladorCliente.getDeportes();
        vista.eventoComboDeporte.removeAllItems();
        vista.participanteComboDeporte.removeAllItems();
        vista.campeonatoComboDeporte.removeAllItems();
        vista.addparticipantesComboDeporte.removeAllItems();
        for (Deporte deporte : listaDeportes) {
            vista.eventoComboDeporte.addItem(deporte);
            vista.participanteComboDeporte.addItem(deporte);
            vista.campeonatoComboDeporte.addItem(deporte);
            vista.addparticipantesComboDeporte.addItem(deporte);
        }
    }

    /**
     * Metodo que crea una lista de usuarios para un cb
     */
    private void listarUsuariosCB() {
        List<Usuario> listaUsuarios = controladorCliente.getUsuarios();
        vista.dlmUsuarios.clear();
        for (Usuario usuario : listaUsuarios) {
            vista.dlmUsuarios.addElement(usuario);
        }
    }

    /**
     * Metodo que crea una lista de roles para un cb
     */
    private void listarRolesCB() {
        List<Rol> listaRoles = controladorCliente.getRoles();
        vista.usuarioComboRol.removeAllItems();
        for (Rol rol : listaRoles) {
            vista.usuarioComboRol.addItem(rol);
        }
    }

    /**
     * Metodo que crea una lista de usuarios
     */
    private void listarUsuarios() {
        List<Usuario> listaUsuarios = controladorCliente.getUsuarios();
        vista.dtmUsuarios.setRowCount(0);
        for (Usuario usuario : listaUsuarios) {
            vista.dtmUsuarios.addRow(Util.usuarioToRow(usuario));
        }
    }

    /**
     * Metodo que crea una lista de apuestas
     *
     * @param user
     */
    private void listarApuestas(Usuario user) {
        List<Apuesta> listaApuestas = controladorCliente.getApuestas(user);
        vista.dtmApuestas.setRowCount(0);
        for (Apuesta apuesta : listaApuestas) {
            vista.dtmApuestas.addRow(Util.apuestaToRow(apuesta));
        }
    }

    /**
     * Metodo que crea una lista de eventos
     */
    private void listarEventos() {
        List<Evento> listaEventos = controladorCliente.getEventos();
        vista.dtmEventos.setRowCount(0);
        for (Evento evento : listaEventos) {
            vista.dtmEventos.addRow(Util.eventoToRow(evento));
        }
    }

    /**
     * Metodo que crea una lista de eventos sin resultado
     */
    private void listarEventosSinResultado() {
        List<Evento> listaEventos = controladorCliente.getEventosTerminados();
        vista.dtmEventosTerminados.setRowCount(0);
        for (Evento evento : listaEventos) {
            if (evento.getParticipantes().size() != 0) {
                if (evento.getParticipantes().get(0).getResultado() == null)
                    vista.dtmEventosTerminados.addRow(Util.eventoToRow(evento));
            }

        }
    }

    /**
     * Metodo que crea una lista de participantes
     */
    private void listarParticipantes() {
        List<Participante> listaParticipantes = controladorCliente.getParticipantes();
        vista.dtmParticipantes.setRowCount(0);
        for (Participante participante : listaParticipantes) {
            vista.dtmParticipantes.addRow(Util.participanteToRow(participante));
        }
    }

    /**
     * Metodo que crea una lista de campeonatos
     */
    private void listarCampeonatos() {
        List<Campeonato> listaCampeonatos = controladorCliente.getCampeonatos();
        vista.dtmCampeonato.setRowCount(0);
        for (Campeonato campeonato : listaCampeonatos) {
            vista.dtmCampeonato.addRow(Util.campeonatoToRow(campeonato));
        }
    }

    /**
     * Metodo que crea una lista de deportes
     */
    private void listarDeportes() {
        List<Deporte> listaDeportes = controladorCliente.getDeportes();
        vista.dtmDeportes.setRowCount(0);
        for (Deporte deporte : listaDeportes) {
            vista.dtmDeportes.addRow(Util.deporteToRow(deporte));
        }
    }

    /**
     * Metodo que crea una lista de roles
     */
    private void listarRoles() {
        List<Rol> listaRoles = controladorCliente.getRoles();

        vista.dtmRoles.setRowCount(0);
        for (Rol rol : listaRoles) {
            vista.dtmRoles.addRow(Util.rolToRow(rol));
        }
    }

    /**
     * Metodo que crea una lista de participantes de un campeonato
     *
     * @param selectedItem
     */
    private void listarParticipantesCampeonato(Object selectedItem) {
        vista.dlmparticipantesCompeticion.clear();
        Campeonato campeonato = (Campeonato) selectedItem;
        List<Participantecampeonato> listarParticipantesCampeonato;
        listarParticipantesCampeonato = controladorCliente.getParticipantesCampeonato(campeonato);
        try {
            if (listarParticipantesCampeonato == null) {
                listarParticipantesCampeonato = controladorCliente.getParticipantesCampeonato(campeonato);
            }
            for (Participantecampeonato participante : listarParticipantesCampeonato) {
                vista.dlmparticipantesCompeticion.addElement(participante);
            }
        }catch (NullPointerException e){
            listarParticipantesCampeonato(selectedItem);
        }

    }

    /**
     * Metodo que crea una lista de participantes de un campeonato
     *
     * @param selectedItem
     */
    private void listarParticipantesCampeonatoEvento(Object selectedItem) {
        Campeonato campeonato = (Campeonato) selectedItem;
        List<Participante> listarParticipantesCampeonato = controladorCliente.getParticipantesCampeonatoEvento(campeonato);
        vista.dlmparticipantesEvento.clear();
        vista.eventoComboLocal.removeAllItems();
        vista.eventoComboVisitante.removeAllItems();
        for (Participante participante : listarParticipantesCampeonato) {
            vista.eventoComboLocal.addItem(participante);
            vista.eventoComboVisitante.addItem(participante);
            vista.dlmparticipantesEvento.addElement(participante);
        }

    }

    /**
     * Metodo que crea una lista de participantes de un deporte
     *
     * @param selectedItem
     */
    private void listarParticipantesDeporte(Object selectedItem) {
        Deporte deporte = (Deporte) selectedItem;
        List<Participante> listaParticipantes = controladorCliente.getParticipantesDeporte(deporte);
        vista.dlmparticipantesDeporte.clear();
        for (Participante participante : listaParticipantes) {
            if (participante.getDeporte() == deporte)
                vista.dlmparticipantesDeporte.addElement(participante);
        }
    }

    /**
     * Metodo que crea una lista de campeonatos de un deporte
     *
     * @param selectedItem
     */
    private void listarCampeonatosDeporte(Object selectedItem) {
        Deporte deporte = (Deporte) selectedItem;
        List<Campeonato> listaCampeonatos = controladorCliente.getCampeonatos();
        vista.addparticipantesComboCompeticion.removeAllItems();
        for (Campeonato campeonato : listaCampeonatos) {
            if (campeonato.getDeporte() == deporte)
                vista.addparticipantesComboCompeticion.addItem(campeonato);
        }
    }

    /**
     * Metodo que crea una lista de campeontaos de un deporte y evento
     *
     * @param selectedItem
     */
    private void listarCampeonatosDeporteEvento(Object selectedItem) {
        Deporte deporte = (Deporte) selectedItem;
        List<Campeonato> listaCampeonatos = controladorCliente.getCampeonatos();
        vista.eventoComboCompeticion.removeAllItems();
        for (Campeonato campeonato : listaCampeonatos) {
            if (campeonato.getDeporte() == deporte)
                vista.eventoComboCompeticion.addItem(campeonato);
        }
    }

    /**
     * Metodo que comprueba si un participante ya esta en una competicion
     *
     * @param p
     * @return
     */
    private boolean comprobarParticipanteCompeticionExiste(Participante p) {

        for (int i = 0; i < vista.dlmparticipantesCompeticion.getSize(); i++) {
            if (vista.dlmparticipantesCompeticion.getElementAt(i).getParticipante().getId() == p.getId())
                return true;
        }
        return false;

    }

    /**
     * Metodo que inserta la cuota manualmente
     */
    private void calcularCuotaManual() {

        vista.eventoCuotaLocal.setText(Util.porcentajeToCuota(vista.eventoSliderPartido.getValue()));
        vista.eventoCuotaVisitante.setText(Util.porcentajeToCuota(100 - vista.eventoSliderPartido.getValue()));

    }

    /**
     * Metodo que calcula la cuota automaticamente
     *
     * @param participante
     * @return
     */
    private Double calcularCuotaIndividual(Participante participante) {

        int totalJugado = participante.getVictorias() + participante.getDerrotas() + participante.getEmpates();
        int probVictoria = participante.getVictorias() * 100 / totalJugado;

        return Util.textToDouble(Util.porcentajeToCuota(probVictoria));

    }

    /**
     * Metodo que calcula la cuota automaticamente
     */
    private void calcularCuotaAutomatica() {

        try {
            Participante pLocal = (Participante) vista.eventoComboLocal.getSelectedItem();
            Participante pVisitante = (Participante) vista.eventoComboVisitante.getSelectedItem();

            int partidosJugadosLocal = pLocal.getVictorias() + pLocal.getDerrotas() + pLocal.getEmpates();
            int probVictoriaLocal = pLocal.getVictorias() * 100 / partidosJugadosLocal;

            int partidosJugadosVisitante = pVisitante.getVictorias() + pVisitante.getDerrotas() + pVisitante.getEmpates();
            int probVictoriaVisitante = pVisitante.getVictorias() * 100 / partidosJugadosVisitante;

            int diferencia = 0;

            if (probVictoriaLocal > probVictoriaVisitante) {
                diferencia = probVictoriaLocal - probVictoriaVisitante;
                vista.eventoSliderPartido.setValue(50 + diferencia);
            } else if (probVictoriaVisitante > probVictoriaLocal) {
                diferencia = probVictoriaVisitante - probVictoriaLocal;
                vista.eventoSliderPartido.setValue(50 - diferencia);
            } else {
                vista.eventoSliderPartido.setValue(50);
            }
        } catch (NullPointerException n) {

        }

    }

    /**
     * Metodo que inserta una imagen de un campeonato
     */
    private void setImageCampeonato() {
        try {
            Image image = null;
            URL url = new URL(vista.campeonatoImageTXT.getText());
            image = ImageIO.read(url);
            Image newimg = image.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH);
            vista.imageCampeonato.setIcon(new ImageIcon(newimg));
        } catch (Exception e) {
        }
    }

    /**
     * Metodo que inserta una imagen de un campeonato
     */
    private void setImageParticipante() {
        try {
            Image image = null;
            URL url = new URL(vista.participanteImageTXT.getText());
            image = ImageIO.read(url);
            Image newimg = image.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH);
            vista.imageParticipante.setIcon(new ImageIcon(newimg));
        } catch (Exception e) {
        }
    }

    /**
     * Metodo que inserta una imagen de un piloto
     */
    private void setImagePiloto(String imagen) {
        try {
            Image image = null;
            URL url = new URL(imagen);
            image = ImageIO.read(url);
            Image newimg = image.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH);
            vista.imagePiloto.setIcon(new ImageIcon(newimg));
        } catch (Exception e) {
        }
    }

    /**
     * Metodo que comprueba si se ha hecho clic en alguna imagen
     *
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {

        vista.panelApuestaPartido.setVisible(false);
        vista.panelApuestaCarrera.setVisible(false);

        for (JPanel p : vista.listaPanelEventos) {
            if (e.getSource() == p) {
                crearPanelApuesta(p.getName());
            }
        }

        if (e.getSource() == vista.image1) {
            crearApuestas(1);
            this.deporteSelected = 1;
        }

        if (e.getSource() == vista.image2) {
            crearApuestas(2);
            this.deporteSelected = 2;
        }

        if (e.getSource() == vista.image3) {
            crearApuestas(3);
            this.deporteSelected = 3;
        }

        if (e.getSource() == vista.image4) {
            crearApuestas(4);
            this.deporteSelected = 4;
        }


    }


    /**
     * Metodo que crea apuestas
     *
     * @param i
     */
    private void crearApuestas(int i) {

        LocalDateTime fecha = vista.calendario.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        vista.paginaApuestas.removeAll();
        vista.listaPanelEventos.clear();

        for (Evento evento : controladorCliente.getEventosDia(fecha)) {
            if (evento.getCampeonato().getDeporte().getId() == i && evento.getCampeonato().getDeporte().getNumParticipantes() == 2) {
                vista.crearEventoScroll(evento);
            } else if (evento.getCampeonato().getDeporte().getId() == i && evento.getCampeonato().getDeporte().getNumParticipantes() != 2) {
                vista.crearEvento2Scroll(evento);
            }
        }

        for (JPanel p : vista.listaPanelEventos) {
            p.addMouseListener(this);
        }

        vista.repaint();

    }

    /**
     * Metodo que crea las apuestas actuales
     */
    private void crearApuestasActuales() {

        vista.paginaApuestasActuales.removeAll();

        List<Apuesta> lista = controladorCliente.getApuestasUsuario(usuario.getId());

        if (lista == null)
            lista = controladorCliente.getApuestasUsuario(usuario.getId());

        for (Apuesta apuesta : lista) {
            if (apuesta.getTerminada() == null) {
                vista.crearApuestaUsuario(apuesta);
            }
        }

        vista.repaint();

    }

    /**
     * Metodo que crea una lista de apuestas de perfil.
     */
    private void crearApuestasPerfil() {

        List<Apuesta> lista = controladorCliente.getApuestasUsuario(usuario.getId());

        vista.panelApuestasPerfil.removeAll();

        for (Apuesta apuesta : lista) {
            if (apuesta.getTerminada() != null && apuesta.getTerminada() == 1) {
                vista.crearApuestaPerfil(apuesta);
            }
        }

        vista.repaint();

    }

    /**
     * Metodo que crea el ranking de usuarios del perfil
     */
    private void crearRanking() {

        List<Usuario> lista = controladorCliente.getRanking();

        vista.panelRankingUsuarios.removeAll();

        vista.crearRankingPerfil(lista, usuario.getId());

        vista.repaint();

    }

    /**
     * Metodo que crea el panel de apuestas al hacer click en un evento
     *
     * @param idEvento
     */
    private void crearPanelApuesta(String idEvento) {


        Evento evento = controladorCliente.getEvento(Integer.parseInt(idEvento));
        if (!Util.byteToCheck(evento.getTerminado())) {
            this.eventoActual = evento;
            List<Participanteevento> participantes = evento.getParticipantes();

            if (evento.getCampeonato().getDeporte().getNumParticipantes() == 2) {
                vista.panelApuestaPartido.setVisible(true);


                try {
                    Image image = null;
                    URL url = new URL(participantes.get(0).getParticipante().getLogo());
                    image = ImageIO.read(url);
                    Image newimg = image.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH);
                    vista.panelApuestaImageLocal.setIcon(new ImageIcon(newimg));
                    url = new URL(participantes.get(1).getParticipante().getLogo());
                    image = ImageIO.read(url);
                    newimg = image.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH);
                    vista.panelApuestaImageVisitante.setIcon(new ImageIcon(newimg));
                } catch (Exception e) {
                }

                vista.panelApuestaLabelLocal.setText(participantes.get(0).getParticipante().getNombre());
                vista.panelApuestaLabelVisitante.setText(participantes.get(1).getParticipante().getNombre());

                vista.panelApuestaCuotaLocal.setText(participantes.get(0).getCuota().toString());
                vista.panelApuestaCuotaVisitante.setText(participantes.get(1).getCuota().toString());

            } else {
                vista.panelApuestaCarrera.setVisible(true);

                vista.dlmparticipantesCarreraApuesta.clear();
                for (Participanteevento participante : participantes) {
                    vista.dlmparticipantesCarreraApuesta.addElement(participante);
                }

            }
        }


    }

    /**
     * Metodo que inserta una apuesta
     *
     * @param parametro
     */
    private void setApuesta(String parametro) {

        switch (parametro) {
            case "local":
                if (comprobarCrearApuestaVacia()) {
                    Util.showWarningAlert("Rellena todos los campos");
                } else if (validarPuntos()) {
                    Apuesta apuesta = new Apuesta();
                    apuesta.setCantidad(Integer.parseInt(vista.panelApuestaPuntosTXT.getText()));
                    apuesta.setEvento(this.eventoActual);
                    apuesta.setApuesta(this.eventoActual.getParticipantes().get(0).getParticipante());
                    apuesta.setUsuario(this.usuario);
                    controladorCliente.insertarApuesta(apuesta);
                    Util.showInfoAlert("Apuesta realizada");
                }
                break;
            case "visitante":
                if (comprobarCrearApuestaVacia()) {
                    Util.showWarningAlert("Rellena todos los campos");
                } else if (validarPuntos()) {
                    Apuesta apuesta = new Apuesta();
                    apuesta.setCantidad(Integer.parseInt(vista.panelApuestaPuntosTXT.getText()));
                    apuesta.setEvento(this.eventoActual);
                    apuesta.setApuesta(this.eventoActual.getParticipantes().get(1).getParticipante());
                    apuesta.setUsuario(this.usuario);
                    controladorCliente.insertarApuesta(apuesta);
                    Util.showInfoAlert("Apuesta realizada");
                }
                break;
            case "carrera":
                if (comprobarCrearApuestaCarreraVacia()) {
                    Util.showWarningAlert("Rellena todos los campos");
                } else if (validarPuntos()) {
                    Apuesta apuesta = new Apuesta();
                    apuesta.setCantidad(Integer.parseInt(vista.panelApuestaPuntosCarreraTXT.getText()));
                    apuesta.setEvento(this.eventoActual);
                    apuesta.setApuesta(this.eventoActual.getParticipantes().get(0).getParticipante());
                    apuesta.setUsuario(this.usuario);
                    controladorCliente.insertarApuesta(apuesta);
                    Util.showInfoAlert("Apuesta realizada");
                }
                break;
        }

    }

    /**
     * Metodo que comprueba que los puntos del usuario son validos
     *
     * @return
     */
    private boolean validarPuntos() {

        int apuesta = 0;

        try {
            if (vista.panelApuestaPartido.isVisible())
                apuesta = Integer.parseInt(vista.panelApuestaPuntosTXT.getText());

            if (vista.panelApuestaCarrera.isVisible())
                apuesta = Integer.parseInt(vista.panelApuestaPuntosCarreraTXT.getText());

        } catch (NumberFormatException e) {
            Util.showWarningAlert("Cantidad introducida no valida");
            return false;
        }

        if (apuesta > this.puntos) {
            Util.showErrorAlert("No tienes suficientes puntos");
            return false;
        }
        if (apuesta < 10) {
            Util.showInfoAlert("La apuesta minima son 10 puntos");
            return false;
        }

        return true;

    }

    /**
     * Metodo que inserta el resultado a un evento
     *
     * @param partido
     */
    private void setResultado(String partido) {

        switch (partido) {
            case "partido":
                if (comprobarResultadoPartidoValido()) {
                    Util.showWarningAlert("Resultado no valido");
                } else {
                    Evento evento = controladorCliente.getEvento((Integer) vista.tablaResultados.getValueAt(vista.tablaResultados.getSelectedRow(), 0));
                    Participanteevento pLocal = evento.getParticipantes().get(0);
                    Participanteevento pVisitante = evento.getParticipantes().get(1);
                    pLocal.setResultado((Integer) vista.resultadoLocalTXT.getValue());
                    pVisitante.setResultado((Integer) vista.resultadoVisitanteTXT.getValue());
                    if (pLocal.getResultado() > pVisitante.getResultado()) {
                        pLocal.setEmpate((byte) 0);
                        pVisitante.setEmpate((byte) 0);
                        pLocal.setVictoria((byte) 1);
                        pVisitante.setVictoria((byte) 0);
                    } else if (pLocal.getResultado() < pVisitante.getResultado()) {
                        pLocal.setEmpate((byte) 0);
                        pVisitante.setEmpate((byte) 0);
                        pLocal.setVictoria((byte) 0);
                        pVisitante.setVictoria((byte) 1);
                    } else {
                        pLocal.setEmpate((byte) 1);
                        pVisitante.setEmpate((byte) 1);
                        pLocal.setVictoria((byte) 0);
                        pVisitante.setVictoria((byte) 0);
                    }
                    List<Participanteevento> lista = new ArrayList<>();
                    lista.add(pLocal);
                    lista.add(pVisitante);
                    controladorCliente.actualizarParticipantes(lista);
                    vista.dtmEventosTerminados.removeRow(vista.tablaResultados.getSelectedRow());
                    vista.resultadoLocalTXT.setValue(0);
                    vista.resultadoVisitanteTXT.setValue(0);
                    vista.panel2Participantes.setVisible(false);
                }

                break;
            case "carrera":
                if (vista.listaGanador.getSelectedIndex() == -1) {
                    Util.showWarningAlert("Selecciona un ganador");
                } else {
                    Evento evento = controladorCliente.getEvento((Integer) vista.tablaResultados.getValueAt(vista.tablaResultados.getSelectedRow(), 0));
                    List<Participanteevento> lista = evento.getParticipantes();
                    for (Participanteevento p : lista) {
                        if (p == vista.dlmGanador.elementAt(vista.listaGanador.getSelectedIndex())) {
                            p.setVictoria((byte) 1);
                            p.setResultado(1);
                        } else {
                            p.setVictoria((byte) 0);
                            p.setResultado(0);
                        }
                    }
                    controladorCliente.actualizarParticipantes(lista);
                    vista.dtmEventosTerminados.removeRow(vista.tablaResultados.getSelectedRow());
                    vista.panelMasParticipantes.setVisible(false);
                }
                break;
        }


    }

    /**
     * Comprueba que el resultado del partido es valido
     *
     * @return
     */
    private boolean comprobarResultadoPartidoValido() {

        try {
            int local = (int) vista.resultadoLocalTXT.getValue();
            int visitante = (int) vista.resultadoVisitanteTXT.getValue();
            if (local < 0 || visitante < 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return true;
        }


    }


    /**
     * Muestra las apuestas de una cuenta.
     *
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        switch (this.deporteSelected) {
            case 1:
                crearApuestas(1);
                break;
            case 2:
                crearApuestas(2);
                break;
            case 3:
                crearApuestas(3);
                break;
            case 4:
                crearApuestas(4);
                break;
        }
    }


    /**
     * Metodo que actualiza las tablas segun el campo de busqueda
     *
     * @param e
     */
    @Override
    public void insertUpdate(DocumentEvent e) {

        if (e.getDocument() == vista.usuarioBusquedaTXT.getDocument()) {
            search(vista.usuarioBusquedaTXT.getText(), vista.sorterUsuarios);
        }
        if (e.getDocument() == vista.apuestaBusquedaTXT.getDocument()) {
            search(vista.apuestaBusquedaTXT.getText(), vista.sorterApuestas);
        }
        if (e.getDocument() == vista.campeonatoBusquedaTXT.getDocument()) {
            search(vista.campeonatoBusquedaTXT.getText(), vista.sorterCampeonato);
        }
        if (e.getDocument() == vista.deporteBusquedaTXT.getDocument()) {
            search(vista.deporteBusquedaTXT.getText(), vista.sorterDeportes);
        }
        if (e.getDocument() == vista.eventoBusquedaTXT.getDocument()) {
            search(vista.eventoBusquedaTXT.getText(), vista.sorterEventos);
        }
        if (e.getDocument() == vista.participanteBusquedaTXT.getDocument()) {
            search(vista.participanteBusquedaTXT.getText(), vista.sorterParticipantes);
        }
        if (e.getDocument() == vista.rolBusquedaTXT.getDocument()) {
            search(vista.rolBusquedaTXT.getText(), vista.sorterRoles);
        }

    }

    /**
     * Metodo que filtra la busqueda
     *
     * @param text
     * @param sorter
     */
    private void search(String text, TableRowSorter sorter) {

        if (text.length() == 0) {
            sorter.setRowFilter(null);
        } else {
            sorter.setRowFilter(RowFilter.regexFilter(text));
        }


    }

    private void ayuda() {

        String[] options = {"Abrir manual", "Descargar para movil"};
        int x = JOptionPane.showOptionDialog(null, "Manual",
                "Ayuda",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        switch (x){
            case 0:
                if (Desktop.isDesktopSupported()) {
                    try {
                        File myFile = new File("src/files/ManualBet4Fun.pdf");
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException  ex) {
                    }
                }
                break;
            case 1:
                showQR();
                break;
        }


    }

    private void showQR() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        ImageIcon icon = new ImageIcon(classLoader.getResource("images/qr.png"));
        JLabel label = new JLabel(icon);
        JPanel panel = new JPanel(new GridBagLayout());
        panel.add(label);
        JPanel panel2 = new JPanel(new BorderLayout());
        panel2.add(panel, BorderLayout.CENTER);
        JOptionPane.showMessageDialog(null, panel2, "Descargar",JOptionPane.DEFAULT_OPTION);

    }


    @Override
    public void mousePressed(MouseEvent e) {
    }
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    @Override
    public void mouseExited(MouseEvent e) {
    }
    @Override
    public void removeUpdate(DocumentEvent e) {
    }
    @Override
    public void changedUpdate(DocumentEvent e) {
    }
}
