package gui;

import cliente.ControladorCliente;
import com.jaimepardo.mapaDB.Usuario;
import hash.Hashear;
import util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.prefs.Preferences;

public class Login extends JFrame implements ActionListener, MouseListener {
    private JPanel panel1;
    private JPanel panelLogin;
    private JLabel imageLogo;
    private JTextField loginUserTXT;
    private JPasswordField loginPassTXT;
    private JButton iniciarSesionButton;
    private JButton registrarsePanelButton;
    private JPanel panelLogin1;
    private JPanel panelLogin2;
    private JButton iniciarSesionPanelButton;
    private JButton registrarseButton;
    private JTextField registroNombreTXT;
    private JTextField registroApellidosTXT;
    private JTextField registroEmailTXT;
    private JTextField registroUserTXT;
    private JPasswordField registroPassTXT;
    private JPasswordField registroPassValidateTXT;
    private JLabel closeButton;
    private JCheckBox remember;

    private ControladorCliente controladorCliente;

    Preferences preference;
    boolean rememberPreference;

    public Login() {
        controladorCliente = new ControladorCliente();
        initFrame();
    }

    /**
     * Inicia los componentes del frames
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400,700);
        this.setUndecorated(true);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
        this.setBackground(UIManager.getColor("nimbusBase"));
        this.panelLogin2.setVisible(false);
        addListeners(this);
        rememberMe();

    }

    /**
     * Crea los componentes de la vista
     */
    private void createUIComponents() {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();


        imageLogo = new JLabel();
        ImageIcon icon = new ImageIcon(classLoader.getResource("images/logo_login.png"));
        Image image = icon.getImage();
        Image newimg = image.getScaledInstance(200, 150,  java.awt.Image.SCALE_SMOOTH);
        imageLogo.setIcon(new ImageIcon(newimg));

        closeButton = new JLabel();
        icon = new ImageIcon(classLoader.getResource("images/close.png"));
        image = icon.getImage();
        closeButton.setIcon(new ImageIcon(image));

    }

    /**
     * Metodo que crea los listeners
     * @param login
     */
    private void addListeners(Login login) {

        this.registrarseButton.addActionListener(login);
        this.registrarseButton.setActionCommand("registrarse");
        this.registrarsePanelButton.addActionListener(login);
        this.registrarsePanelButton.setActionCommand("panelRegistrarse");
        this.iniciarSesionButton.addActionListener(login);
        this.iniciarSesionButton.setActionCommand("login");
        this.iniciarSesionPanelButton.addActionListener(login);
        this.iniciarSesionPanelButton.setActionCommand("panelLogin");
        this.closeButton.addMouseListener(login);

    }

    /**
     * Metodo que controla los botones pulsados
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()){
            case "panelLogin":
                this.panelLogin1.setVisible(true);
                this.panelLogin2.setVisible(false);
                break;
            case "panelRegistrarse":
                this.panelLogin1.setVisible(false);
                this.panelLogin2.setVisible(true);
                break;
            case "registrarse":
                registrarse();
                break;
            case "login":
                iniciarSesion();
                break;
        }

    }

    /**
     * Metodo que inicia sesion
     */
    private void iniciarSesion() {

        if (this.loginUserTXT.getText().isEmpty() || this.loginPassTXT.getPassword().equals("")){
            Util.showInfoAlert("Rellena los campos");
        }else if(!validatePassword(loginPassTXT.getPassword())){
            Util.showInfoAlert("La contraseña debe tener mas de 8 caracteres mayusculas, minusculas, y numeros");
        }else {
            Usuario usuario = new Usuario();
            usuario.setUsername(this.loginUserTXT.getText());
            String password = Hashear.hashear(String.valueOf(this.loginPassTXT.getPassword()));
            usuario.setPass(password);

            boolean continuar = controladorCliente.iniciarSesion(usuario);

            if (continuar){
                saveRemember();
                //TEMPORAL
                Vista vista = new Vista(controladorCliente.sessionUser.getRol(), controladorCliente);
                Controlador controlador = new Controlador(vista, controladorCliente);
                this.dispose();
                //TEMPORAL
            }else {
                Util.showWarningAlert("Usuario o contrasena incorrectos");
            }

        }

    }

    /**
     * Metodo que guarda la contraseña
     */
    public void saveRemember(){

                if (remember.isSelected() && !rememberPreference){
                    // Insert into the preference the name
                    preference.put("User", loginUserTXT.getText());
                    preference.put("Password", String.valueOf(this.loginPassTXT.getPassword()));
                    preference.putBoolean("rememberMe", true);
                } else if (!remember.isSelected() && rememberPreference){
                    // Reset the preference
                    preference.put("User", "");
                    preference.put("Password", "");
                    preference.putBoolean("rememberMe", false);
                }
    }

    /**
     * Metodo que guarda la contraseña
     */
    public void rememberMe(){
        preference = Preferences.userNodeForPackage(this.getClass());
        // Put the boolean of the rememberMe preference
        rememberPreference = preference.getBoolean("rememberMe", Boolean.valueOf(""));
        // Check if the check box was selected
        if (rememberPreference){
            // Replace the textField by the preference User and Password who will be stock
            loginUserTXT.setText(preference.get("User", ""));
            loginPassTXT.setText(preference.get("Password", ""));
            remember.setSelected(rememberPreference);
        }
    }

    /**
     * Metodo que registra un usuario
     */
    private void registrarse() {

        if (comprobarCamposRegistroVacios()){
            Util.showInfoAlert("Rellena todos los campos");
        }else if (!String.valueOf(registroPassTXT.getPassword()).equals(String.valueOf(registroPassValidateTXT.getPassword()))) {
            Util.showWarningAlert("Las contrasenas no coinciden");
        }else if(!validatePassword(registroPassTXT.getPassword())){
            Util.showInfoAlert("La contraseña debe tener mas de 8 caracteres mayusculas, minusculas, y numeros");
        }else {
            Usuario usuario = new Usuario();
            usuario.setNombre(registroNombreTXT.getText());
            usuario.setApellidos(registroApellidosTXT.getText());
            usuario.setEmail(registroEmailTXT.getText());
            usuario.setUsername(registroUserTXT.getText());
            String password = Hashear.hashear(String.valueOf(this.registroPassTXT.getPassword()));
            usuario.setPass(password);
            if (controladorCliente.registrarUsuario(usuario)){
                Util.showInfoAlert("Usuario registrado correctamente");
                borrarCampos();
            }else {
                Util.showErrorAlert("Ese nombre de usuario ya existe");
            }

        }

    }

    /**
     * Metodo que comprueba que estan los campos rellenados
     * @return
     */
    private boolean comprobarCamposRegistroVacios() {

        return registroNombreTXT.getText().isEmpty() ||
                registroApellidosTXT.getText().isEmpty() ||
                registroEmailTXT.getText().isEmpty() ||
                registroUserTXT.getText().isEmpty() ||
                registroPassValidateTXT.getPassword().length==0 ||
                registroPassTXT.getPassword().length==0;

    }

    /**
     * Metodo que borra los campos
     */
    private void borrarCampos() {

        registroNombreTXT.setText("");
        registroApellidosTXT.setText("");
        registroEmailTXT.setText("");
        registroUserTXT.setText("");
        registroPassValidateTXT.setText("");
        registroPassTXT.setText("");

    }

    /**
     * Metodo que comprueba que la contraseña cumple los requisitos.
     * @param password
     * @return
     */
    private boolean validatePassword(char[] password) {

        boolean mayusculas = false;
        boolean minusculas = false;
        boolean numeros = false;

        for (int i=0 ; i<password.length ; i++){
            if (Character.isUpperCase(password[i]))
                mayusculas = true;

            if (Character.isLowerCase(password[i]))
                minusculas = true;

            if (Character.isDigit(password[i]))
                numeros = true;
        }

        if (mayusculas && minusculas && numeros && password.length>=8){
            return true;
        }else {
            return false;
        }

    }

    /**
     * Metodo que compruebas si se ha hecho click en cerrar
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getSource().equals(closeButton)){
            controladorCliente.desconectar();
            System.exit(0);
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
