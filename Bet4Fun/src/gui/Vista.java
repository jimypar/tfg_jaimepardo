package gui;

import cliente.ControladorCliente;
import com.github.lgooddatepicker.components.DateTimePicker;
import com.jaimepardo.mapaDB.*;
import com.toedter.calendar.JCalendar;
import util.Util;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Vista extends JFrame implements MouseListener, MouseMotionListener, WindowListener {
    private final static String TITULOFRAME="Bet4Fun";
    private JPanel panel;
    private JPanel deportes;

    JLabel minimizeButton;
    JLabel closeButton;
    JLabel maximizeButton;

    JLabel logo;
    JLabel image1;
    JLabel image2;
    JLabel image3;
    JLabel image4;
    JTabbedPane tabbedPanelPrincipal;


    JButton miPerfilButton;
    JLabel userPuntosTXT;

    //APUESTAS
    JCalendar calendario;

    //PANEL CONFIGURACION BBDD
    JTabbedPane tabbedPanelBBDD;

    //Usuarios
    JButton usuarioAddButton;
    JButton usuarioDelButton;
    JButton usuarioModButton;
    JTextField usuarioNombreTXT;
    JTextField usuarioApellidoTXT;
    JTextField usuarioNickTXT;
    JTextField usuarioPassTXT;
    JTextField usuarioCorreoTXT;
    JTextField usuarioPuntosTXT;
    JTable usuariosTabla;

    //Apuestas
    JTextField apuestaCantidadTXT;
    JCheckBox apuestaCheckGanada;
    JCheckBox apuestaCheckTerminada;
    JTable apuestasTabla;
    JList apuestaListaUsuarios;
    JList apuestaListaParticipantes;
    JButton apuestaDelButton;
    JButton apuestaModButton;
    JLabel apuestaEventoTXT;

    //Eventos
    JCheckBox eventoCheckTerminado;
    JComboBox eventoComboDeporte;
    JTable eventosTabla;
    JComboBox eventoComboVisitante;
    JSlider eventoSliderPartido;
    JButton eventoAddButton;
    DateTimePicker eventoDatePicker;
    JComboBox eventoComboCompeticion;
    JPanel eventoPanel2Equipos;
    JButton eventoDelButton;
    JButton eventoModButton;
    JComboBox eventoComboLocal;
    JComboBox usuarioComboRol;
    JScrollPane eventoPanelMasParticipantes;
    JList eventoListaParticipantes;
    JLabel eventoCuotaLocal;
    JLabel eventoCuotaVisitante;

    //Participantes
    JTextField participanteNombreTXT;
    JTextField participanteVictoriasTXT;
    JTable participantesTabla;
    JComboBox participanteComboDeporte;
    JButton participanteAddButton;
    JButton participanteDelButton;
    JButton participanteModButton;
    JTextField participanteImageTXT;
    JTextField participanteDerrotasTXT;
    JTextField participanteEmpatesTXT;
    JButton participanteCargarButton;
    JLabel imageParticipante;

    //Campeonatos
    JComboBox campeonatoComboAnno;
    JButton campeonatoAddButton;
    JButton campeonatoDelButton;
    JButton campeonatoModButton;
    JComboBox campeonatoComboDeporte;
    JTextField campeonatoNombreTXT;
    JTextField campeonatoImageTXT;
    JTable campeonatosTabla;
    JButton campeonatoCargarButton;
    JLabel imageCampeonato;

    //Add Participantes
    JComboBox addparticipantesComboDeporte;
    JComboBox addparticipantesComboCompeticion;
    JButton addparticipantesInsertarButton;
    JButton addparticipantesRetirarButton;
    JList addparticipantesList1;
    JList addparticipantesList2;

    //Roles
    JTextField rolNombreTXT;
    JCheckBox rolCheckApostar;
    JTable rolesTabla;
    JButton rolAddButton;
    JButton rolDelButton;
    JButton rolModButton;
    JCheckBox rolCheckResultados;
    JCheckBox rolCheckPartidos;
    JCheckBox rolCheckUsuarios;

    //Deportes
    JTextField deporteNombreTXT;
    JButton deporteAddButton;
    JTable deportesTabla;
    JButton deporteDelButton;
    JButton deporteModButton;
    JTextField deporteParticipantesTXT;


    //Busquedas
    JTextField usuarioBusquedaTXT;
    JTextField apuestaBusquedaTXT;
    JTextField eventoBusquedaTXT;
    JTextField participanteBusquedaTXT;
    JTextField campeonatoBusquedaTXT;
    JTextField deporteBusquedaTXT;
    JTextField rolBusquedaTXT;

    JPanel paginaApuestas;
    JPanel paginaApuestasActuales;

    //Panel Apuesta
    JPanel panelApuestaPartido;
    JLabel panelApuestaImageLocal;
    JLabel panelApuestaImageVisitante;
    JLabel panelApuestaLabelLocal;
    JLabel panelApuestaLabelVisitante;
    JTextField panelApuestaPuntosTXT;
    JButton panelApuestaLocalButton;
    JLabel panelApuestaCuotaLocal;
    JLabel panelApuestaCuotaVisitante;
    JButton panelApuestaVisitanteButton;
    JButton cerrarPanel2;


    JPanel panelApuestaCarrera;
    JTextField panelApuestaPuntosCarreraTXT;
    JButton panelApuestaButton;
    JList listaPilotosCarrera;
    JLabel apostarCuotaCarreraTXT;
    JLabel apostarPilotoCarreraTXT;
    JLabel imagePiloto;


    //Panel Resultados
    JTable tablaResultados;
    JSpinner resultadoLocalTXT;
    JSpinner resultadoVisitanteTXT;
    JButton marcarResultado2EquiposButton;
    JButton marcarResultadoCarreraButton;
    JList listaGanador;
    JPanel panel2Participantes;
    JPanel panelMasParticipantes;
    JLabel resultadoEquipo1;
    JLabel resultadoEquipo2;
    JPanel panelCerrar;

    //Perfil
    Component panelPerfil;
    JLabel perfilUsername;
    JLabel perfilPuntos;
    JLabel perfilNombre;
    JLabel perfilApellidos;
    JLabel perfilEmail;
    JPanel panelApuestasPerfil;
    JPanel panelRankingUsuarios;
    JButton ayudaButton;


    List<JPanel> listaPanelEventos;

    //DTMS
    DefaultTableModel dtmUsuarios;
    DefaultTableModel dtmApuestas;
    DefaultTableModel dtmEventos;
    DefaultTableModel dtmEventosTerminados;
    DefaultTableModel dtmParticipantes;
    DefaultTableModel dtmCampeonato;
    DefaultTableModel dtmDeportes;
    DefaultTableModel dtmRoles;

    DefaultListModel<Usuario> dlmUsuarios;
    DefaultListModel<Participante> dlmparticipantesDeporte;
    DefaultListModel<Participantecampeonato> dlmparticipantesCompeticion;
    DefaultListModel<Participante> dlmparticipantesEvento;
    DefaultListModel<Participante> dlmparticipantesApuesta;
    DefaultListModel<Participanteevento> dlmparticipantesCarreraApuesta;
    DefaultListModel<Participanteevento> dlmGanador;

    TableRowSorter sorterUsuarios;
    TableRowSorter sorterApuestas;
    TableRowSorter sorterEventos;
    TableRowSorter sorterParticipantes;
    TableRowSorter sorterCampeonato;
    TableRowSorter sorterDeportes;
    TableRowSorter sorterRoles;

    private int mouseX;
    private int mouseY;

    ControladorCliente controladorCliente;


    public Vista(Rol rol, ControladorCliente controladorCliente) {
        this.controladorCliente = controladorCliente;
        initFrame();
        setPermisos(rol);
    }

    /**
     * Metodo que muestra unos paneles segun los permisos del usuario
      * @param rol
     */
    private void setPermisos(Rol rol) {



        if (!Util.byteToCheck(rol.getPermisoModificarPartidos()) && !Util.byteToCheck(rol.getPermisoModificarUsuarios())){
            tabbedPanelPrincipal.remove(2);
        }else {
            if (!Util.byteToCheck(rol.getPermisoModificarUsuarios())){
                tabbedPanelBBDD.remove(7);
                tabbedPanelBBDD.remove(6);
                tabbedPanelBBDD.remove(1);
                tabbedPanelBBDD.remove(0);
            }
            if (!Util.byteToCheck(rol.getPermisoModificarPartidos())){
                tabbedPanelBBDD.remove(5);
                tabbedPanelBBDD.remove(4);
                tabbedPanelBBDD.remove(3);
                tabbedPanelBBDD.remove(2);
            }
        }

        if (!Util.byteToCheck(rol.getPermisoModificarResultados())) {
            tabbedPanelPrincipal.remove(1);
        }
        if (!Util.byteToCheck(rol.getPermisoApuesta())) {
            tabbedPanelPrincipal.remove(0);
        }


    }

    /**
     * Inicia los componentes del frames
     */
    public void initFrame() {
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1250,700);
        this.setUndecorated(true);
        panelCerrar.setVisible(true);
        panelCerrar.addMouseMotionListener(this);
        panelCerrar.addMouseListener(this);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        tabbedPanelPrincipal.setBackground(UIManager.getColor("nimbusBase"));
        listaPanelEventos= new ArrayList<>();
        panelApuestaPartido.setVisible(false);
        panelApuestaCarrera.setVisible(false);
        panel2Participantes.setVisible(false);
        panelMasParticipantes.setVisible(false);
        Image icon = Toolkit.getDefaultToolkit().getImage("src\\images\\logo_white_ke5_icon.png");
        this.setIconImage(icon);

        initComponents();
        setTableModels();
        setListModels();
    }


    /**
     * Inicia componentes
     */
    private void initComponents() {

        panelPerfil = tabbedPanelPrincipal.getComponentAt(3);
        tabbedPanelPrincipal.removeTabAt(3);

    }

    /**
     * Crea un tablemodel por cada tabla
     */
    private void setTableModels() {
        this.dtmUsuarios=new DefaultTableModel();
        dtmUsuarios.setColumnIdentifiers(new String[] {"ID", "Nombre", "Apellidos", "Usuario", "Password", "Correo", "Puntos","Rol"});
        this.usuariosTabla.setModel(dtmUsuarios);
        sorterUsuarios = new TableRowSorter<>(dtmUsuarios);
        this.usuariosTabla.setRowSorter(sorterUsuarios);

        this.dtmApuestas=new DefaultTableModel();
        dtmApuestas.setColumnIdentifiers(new String[] {"ID", "Usuario", "Evento", "Apuesta", "Cantidad", "Ganada", "Terminada"});
        this.apuestasTabla.setModel(dtmApuestas);
        sorterApuestas = new TableRowSorter<>(dtmApuestas);
        this.apuestasTabla.setRowSorter(sorterApuestas);

        this.dtmEventos=new DefaultTableModel();
        dtmEventos.setColumnIdentifiers(new String[] {"ID", "Deporte","Competicion", "Fecha", "Terminado"});
        this.eventosTabla.setModel(dtmEventos);
        sorterEventos = new TableRowSorter<>(dtmEventos);
        this.eventosTabla.setRowSorter(sorterEventos);

        this.dtmEventosTerminados=new DefaultTableModel();
        dtmEventosTerminados.setColumnIdentifiers(new String[] {"ID", "Deporte","Competicion", "Fecha", "Terminado"});
        this.tablaResultados.setModel(dtmEventosTerminados);

        this.dtmParticipantes=new DefaultTableModel();
        dtmParticipantes.setColumnIdentifiers(new String[] {"ID", "Deporte", "Nombre", "Victorias", "Derrotas", "Empates", "Imagen"});
        this.participantesTabla.setModel(dtmParticipantes);
        sorterParticipantes = new TableRowSorter<>(dtmParticipantes);
        this.participantesTabla.setRowSorter(sorterParticipantes);

        this.dtmCampeonato=new DefaultTableModel();
        dtmCampeonato.setColumnIdentifiers(new String[] {"ID", "Deporte", "Nombre", "Year", "Imagen"});
        this.campeonatosTabla.setModel(dtmCampeonato);
        sorterCampeonato = new TableRowSorter<>(dtmCampeonato);
        this.campeonatosTabla.setRowSorter(sorterCampeonato);

        this.dtmDeportes=new DefaultTableModel();
        dtmDeportes.setColumnIdentifiers(new String[] {"ID", "Nombre", "Participantes"});
        this.deportesTabla.setModel(dtmDeportes);
        sorterDeportes = new TableRowSorter<>(dtmDeportes);
        this.deportesTabla.setRowSorter(sorterDeportes);

        this.dtmRoles=new DefaultTableModel();
        dtmRoles.setColumnIdentifiers(new String[] {"ID", "Nombre", "Permiso apuesta", "Permiso resultados", "Permiso partidos", "Permiso usuarios"});
        this.rolesTabla.setModel(dtmRoles);
        sorterRoles = new TableRowSorter<>(dtmRoles);
        this.rolesTabla.setRowSorter(sorterRoles);

        paginaApuestas.setLayout(new BoxLayout(paginaApuestas, BoxLayout.PAGE_AXIS));
        paginaApuestasActuales.setLayout(new BoxLayout(paginaApuestasActuales, BoxLayout.PAGE_AXIS));
        panelApuestasPerfil.setLayout(new BoxLayout(panelApuestasPerfil, BoxLayout.PAGE_AXIS));
        panelRankingUsuarios.setLayout(new BoxLayout(panelRankingUsuarios, BoxLayout.PAGE_AXIS));

    }

    /**
     * Crea un listmodel por cada lista
     */
    private void setListModels() {
        this.dlmUsuarios=new DefaultListModel<>();
        this.apuestaListaUsuarios.setModel(dlmUsuarios);
        this.dlmparticipantesDeporte=new DefaultListModel<>();
        this.addparticipantesList1.setModel(dlmparticipantesDeporte);
        this.dlmparticipantesCompeticion=new DefaultListModel<>();
        this.addparticipantesList2.setModel(dlmparticipantesCompeticion);
        this.dlmparticipantesEvento=new DefaultListModel<>();
        this.eventoListaParticipantes.setModel(dlmparticipantesEvento);
        this.dlmparticipantesApuesta=new DefaultListModel<>();
        this.apuestaListaParticipantes.setModel(dlmparticipantesApuesta);
        this.dlmparticipantesCarreraApuesta=new DefaultListModel<>();
        this.listaPilotosCarrera.setModel(dlmparticipantesCarreraApuesta);
        this.dlmGanador=new DefaultListModel<>();
        this.listaGanador.setModel(dlmGanador);

    }

    /**
     * Crea componentes graficos como botones, imnagenes o el calendario
     */
    private void createUIComponents() {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        closeButton = new JLabel();
        closeButton.setIcon(new ImageIcon(classLoader.getResource("images/close.png")));
        closeButton.addMouseListener(this);

        minimizeButton = new JLabel();
        minimizeButton.setIcon(new ImageIcon(classLoader.getResource("images/minimize.png")));
        minimizeButton.addMouseListener(this);

        maximizeButton = new JLabel();
        maximizeButton.setIcon(new ImageIcon(classLoader.getResource("images/maximize.png")));
        maximizeButton.addMouseListener(this);



        tabbedPanelPrincipal = new JTabbedPane();
        tabbedPanelBBDD = new JTabbedPane();

        calendario = new JCalendar();
        calendario.setWeekOfYearVisible(false);
        calendario.setDecorationBordersVisible(true);
        calendario.setSize(30,30);

        campeonatoComboAnno = new JComboBox();
        for(int i=2000;i<=2030;i++){
            campeonatoComboAnno.addItem(i);
        }
        campeonatoComboAnno.setSelectedIndex(22);


        ImageIcon imageIcon = new ImageIcon(classLoader.getResource("images/logo_white.png")); // load the image to a imageIcon
        Image image = imageIcon.getImage(); // transform it
        Image newimg = image.getScaledInstance(50, 50,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon = new ImageIcon(newimg);  // transform it back
        logo = new JLabel(imageIcon);
        logo.addMouseListener(this);

        image1 = new JLabel(new ImageIcon(classLoader.getResource("images/Soccerball.png")));
        image1.addMouseListener(this);
        image2 = new JLabel(new ImageIcon(classLoader.getResource("images/Basketball.png")));
        image2.addMouseListener(this);
        image3 = new JLabel(new ImageIcon(classLoader.getResource("images/Tennisball.png")));
        image3.addMouseListener(this);
        image4 = new JLabel(new ImageIcon(classLoader.getResource("images/F1ball.png")));
        image4.addMouseListener(this);

    }

    /**
     * Metodo que comprueba si se ha clicado un componente
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        if (e.getSource() == closeButton){
            controladorCliente.desconectar();
            System.exit(0);
        }
        if (e.getSource() == minimizeButton){
            this.setExtendedState(this.ICONIFIED);
        }
        if (e.getSource() == maximizeButton) {
            if (this.getExtendedState() != this.MAXIMIZED_BOTH){
                this.setExtendedState(this.MAXIMIZED_BOTH);
                maximizeButton.setIcon(new ImageIcon(classLoader.getResource("images/maximize2.png")));
            }else{
                this.setExtendedState(this.NORMAL);
                maximizeButton.setIcon(new ImageIcon(classLoader.getResource("images/maximize.png")));
            }
        }


    }


    @Override
    public void mousePressed(MouseEvent e) {

        mouseX = e.getX();
        mouseY = e.getY();

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Metodo que comprueba si se ha entrado a un componente
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        if (this.listaPanelEventos!=null){
            for (JPanel panel:this.listaPanelEventos) {
                if (e.getSource() == panel)
                    panel.setBackground(Color.DARK_GRAY);
            }
        }

        if(e.getSource() == this.image1)
        image1.setIcon(new ImageIcon(classLoader.getResource("images/SoccerballSelected.png")));

        if(e.getSource() == this.image2)
        image2.setIcon(new ImageIcon(classLoader.getResource("images/BasketballSelected.png")));

        if(e.getSource() == this.image3)
        image3.setIcon(new ImageIcon(classLoader.getResource("images/TennisballSelected.png")));

        if(e.getSource() == this.image4)
        image4.setIcon(new ImageIcon(classLoader.getResource("images/F1ballSelected.png")));


    }

    /**
     * Metodo que comprueba si se ha salido del componente
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        if (this.listaPanelEventos!=null){
            for (JPanel panel:this.listaPanelEventos) {
                if (e.getSource() == panel)
                    panel.setBackground(UIManager.getColor("control"));
            }
        }

        if(e.getSource() == this.image1)
            image1.setIcon(new ImageIcon(classLoader.getResource("images/Soccerball.png")));

        if(e.getSource() == this.image2)
            image2.setIcon(new ImageIcon(classLoader.getResource("images/Basketball.png")));

        if(e.getSource() == this.image3)
            image3.setIcon(new ImageIcon(classLoader.getResource("images/Tennisball.png")));

        if(e.getSource() == this.image4)
            image4.setIcon(new ImageIcon(classLoader.getResource("images/F1ball.png")));

    }

    /**
     * Metodo que crea los eventos
     * @param evento
     */
    public void crearEventoScroll(Evento evento){

        List<Participanteevento> participantes = evento.getParticipantes();
        Participante local = participantes.get(0).getParticipante();
        Participante visitante = participantes.get(1).getParticipante();

        JPanel partido = new JPanel();
        partido.setName(evento.getId()+"");
        partido.setMaximumSize(new Dimension(1000, 60));
        listaPanelEventos.add(partido);


        JLabel imageCompeticion = new JLabel();
        JLabel fecha = new JLabel(" "+evento.getFecha().format(DateTimeFormatter.ofPattern("HH:mm")));
        fecha.setFont(new Font("UD Digi Kyokasho NK-B", Font.ITALIC, 18));
        JLabel imageLocal = new JLabel();
        JLabel equipo1 = new JLabel(local.getNombre());
        equipo1.setPreferredSize(new Dimension(250, 20));
        equipo1.setHorizontalAlignment(SwingConstants.RIGHT);
        equipo1.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 18));
        JLabel resultado;
        if (evento.getTerminado()==1 && participantes.get(0).getResultado()!=null){
            resultado = new JLabel(participantes.get(0).getResultado()+" - "+participantes.get(1).getResultado());
        }else {
            resultado = new JLabel(" - ");
            partido.addMouseListener(this);
        }
        resultado.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 18));
        JLabel equipo2 = new JLabel(visitante.getNombre());
        equipo2.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 18));
        equipo2.setHorizontalAlignment(SwingConstants.LEFT);
        equipo2.setPreferredSize(new Dimension(250, 20));
        JLabel imageVisitante = new JLabel();

        try {
            Image image;
            URL url = new URL(local.getLogo());
            image = ImageIO.read(url);
            Image newimg = image.getScaledInstance(50, 50,  java.awt.Image.SCALE_SMOOTH);
            imageLocal.setIcon(new ImageIcon(newimg));
            url = new URL(visitante.getLogo());
            image = ImageIO.read(url);
            newimg = image.getScaledInstance(50, 50,  java.awt.Image.SCALE_SMOOTH);
            imageVisitante.setIcon(new ImageIcon(newimg));
            url = new URL(evento.getCampeonato().getLogo());
            image = ImageIO.read(url);
            newimg = image.getScaledInstance(30, 30,  java.awt.Image.SCALE_SMOOTH);
            imageCompeticion.setIcon(new ImageIcon(newimg));
        } catch (Exception e) {
        }


        partido.add(imageCompeticion);
        partido.add(fecha);
        partido.add(equipo1);
        partido.add(imageLocal);
        partido.add(resultado);
        partido.add(imageVisitante);
        partido.add(equipo2);

        paginaApuestas.add(partido);
        paginaApuestas.add(Box.createRigidArea(new Dimension(0,10)));

    }

    /**
     * Metodo que crea los eventos de 2
     * @param evento
     */
    public void crearEvento2Scroll(Evento evento) {

        List<Participanteevento> participantes = evento.getParticipantes();

        JPanel partido = new JPanel();
        partido.setLayout(new BoxLayout(partido, BoxLayout.PAGE_AXIS));
        partido.setName(evento.getId()+"");
        listaPanelEventos.add(partido);
        partido.addMouseListener(this);
        JLabel nombreEvento = new JLabel("Carrera : "+evento.getCampeonato());
        nombreEvento.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 18));
        nombreEvento.setHorizontalAlignment(SwingConstants.CENTER);
        partido.add(nombreEvento);

        for (Participanteevento participanteevento: participantes) {

            JPanel panelParticipante = new JPanel();
            panelParticipante.setMaximumSize(new Dimension(750, 30));
            JLabel imagen = new JLabel();
            try {
                Image image;
                URL url = new URL(participanteevento.getParticipante().getLogo());
                image = ImageIO.read(url);
                Image newimg = image.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH);
                imagen.setIcon(new ImageIcon(newimg));
            }catch (Exception e){
            }

            JLabel nombre = new JLabel(participanteevento.getParticipante().getNombre());
            panelParticipante.add(imagen);
            panelParticipante.add(nombre);
            panelParticipante.setOpaque(false);
            partido.add(panelParticipante);

        }
        partido.setMaximumSize(new Dimension(750, (participantes.size()*30)+20));
        paginaApuestas.add(partido);
        paginaApuestas.add(Box.createRigidArea(new Dimension(0,10)));

    }

    /**
     * Metodo que crea las apuestas del usuario
     * @param apuesta
     */
    public void crearApuestaUsuario(Apuesta apuesta){

        double cuota=0;

        for (Participanteevento p:apuesta.getEvento().getParticipantes()) {
            if (p.getParticipante() == apuesta.getApuesta()){
                cuota = p.getCuota();
            }
        }

        JPanel panel = new JPanel();

        JPanel panelApuesta = new JPanel();
        panelApuesta.setLayout(new BoxLayout(panelApuesta, BoxLayout.Y_AXIS));

        JLabel equipoApostado = new JLabel(apuesta.getApuesta()+ "  Cuota: "+cuota);
        equipoApostado.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 15));

        JLabel ganancias = new JLabel("Puntos: "+apuesta.getCantidad()+"  Obtienes: "+Integer.valueOf((int) (apuesta.getCantidad() *cuota)));
        ganancias.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 15));

        panelApuesta.add(equipoApostado);
        panelApuesta.add(ganancias);
        panelApuesta.setBackground(Color.DARK_GRAY);

        panel.add(panelApuesta);
        panel.setBackground(Color.DARK_GRAY);

        paginaApuestasActuales.add(panel);
        paginaApuestasActuales.add(Box.createRigidArea(new Dimension(0,10)));

    }

    /**
     * Metodo que crea las apuestas del perfil
     * @param apuesta
     */
    public void crearApuestaPerfil(Apuesta apuesta){

        double cuota=0;

        for (Participanteevento p:apuesta.getEvento().getParticipantes()) {
            if (p.getParticipante() == apuesta.getApuesta()){
                cuota = p.getCuota();
            }
        }

        JPanel panel = new JPanel();
        panel.setMaximumSize(new Dimension(750, 60));

        JLabel equipoApostado = new JLabel(apuesta.getApuesta()+ "  Cuota: "+cuota);
        equipoApostado.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 15));
        equipoApostado.setHorizontalAlignment(JLabel.CENTER);
        equipoApostado.setVerticalAlignment(JLabel.CENTER);

        JLabel ganancias = new JLabel("Puntos: "+apuesta.getCantidad()+"  Obtienes: "+Integer.valueOf((int) (apuesta.getCantidad() *cuota)));
        ganancias.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 15));
        ganancias.setHorizontalAlignment(JLabel.CENTER);
        ganancias.setVerticalAlignment(JLabel.CENTER);

        panel.add(equipoApostado);
        panel.add(ganancias);

        if (apuesta.getGanada()==1){
            panel.setBackground(new Color(67,100, 67));
        }else{
            panel.setBackground(new Color(100, 47, 50));
        }


        panelApuestasPerfil.add(panel);
        panelApuestasPerfil.add(Box.createRigidArea(new Dimension(0,10)));

    }

    /**
     * Metodo que crea el ranking de usuarios
     * @param usuarios
     * @param id
     */
    public void crearRankingPerfil(List<Usuario> usuarios, int id){

        for (int i = 0; i<usuarios.size() ; i++){

            JPanel panel = new JPanel();
            panel.setMaximumSize(new Dimension(750, 60));

            JLabel usuario = new JLabel((i+1)+" : "+usuarios.get(i).getUsername());
            usuario.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 15));

            JLabel puntos = new JLabel("Puntos: "+ usuarios.get(i).getPuntos());
            puntos.setFont(new Font("UD Digi Kyokasho NK-B", Font.PLAIN, 15));

            panel.add(usuario);
            panel.add(puntos);

            if (usuarios.get(i).getId()==id){
                panel.setBackground(new Color(100, 97, 47));
            }

            panelRankingUsuarios.add(panel);
            panelRankingUsuarios.add(Box.createRigidArea(new Dimension(0,10)));

        }


    }

    /**
     * Metodo que permite arrastra la pestaña con la franja superior
     * @param e
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (this.getExtendedState() != this.MAXIMIZED_BOTH){
            this.setLocation(this.getX() + e.getX() - mouseX, this.getY() + e.getY() - mouseY);
        }

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        controladorCliente.desconectar();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

