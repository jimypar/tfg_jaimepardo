package com.jaimepardo.mapaDB;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
public class Rol implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private String descripcion;
    private Byte permisoApuesta;
    private Byte permisoModificarResultados;
    private Byte permisoModificarPartidos;
    private Byte permisoModificarUsuarios;
    private List<Usuario> usuarios;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "permisoApuesta")
    public Byte getPermisoApuesta() {
        return permisoApuesta;
    }

    public void setPermisoApuesta(Byte permisoApuesta) {
        this.permisoApuesta = permisoApuesta;
    }

    @Basic
    @Column(name = "permisoModificarResultados")
    public Byte getPermisoModificarResultados() {
        return permisoModificarResultados;
    }

    public void setPermisoModificarResultados(Byte permisoModificarResultados) {
        this.permisoModificarResultados = permisoModificarResultados;
    }

    @Basic
    @Column(name = "permisoModificarPartidos")
    public Byte getPermisoModificarPartidos() {
        return permisoModificarPartidos;
    }

    public void setPermisoModificarPartidos(Byte permisoModificarPartidos) {
        this.permisoModificarPartidos = permisoModificarPartidos;
    }

    @Basic
    @Column(name = "permisoModificarUsuarios")
    public Byte getPermisoModificarUsuarios() {
        return permisoModificarUsuarios;
    }

    public void setPermisoModificarUsuarios(Byte permisoModificarUsuarios) {
        this.permisoModificarUsuarios = permisoModificarUsuarios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rol rol = (Rol) o;
        return id == rol.id &&
                Objects.equals(descripcion, rol.descripcion) &&
                Objects.equals(permisoApuesta, rol.permisoApuesta) &&
                Objects.equals(permisoModificarResultados, rol.permisoModificarResultados) &&
                Objects.equals(permisoModificarPartidos, rol.permisoModificarPartidos) &&
                Objects.equals(permisoModificarUsuarios, rol.permisoModificarUsuarios);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, descripcion, permisoApuesta, permisoModificarResultados, permisoModificarPartidos, permisoModificarUsuarios);
    }

    @OneToMany(mappedBy = "rol")
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public String toString() {
        return id + " " + descripcion;
    }
}
