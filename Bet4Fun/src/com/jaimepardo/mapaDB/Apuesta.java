package com.jaimepardo.mapaDB;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Apuesta implements Serializable {
    private int id;
    private int cantidad;
    private Byte ganada;
    private Byte terminada;
    private Usuario usuario;
    private Evento evento;
    private Participante apuesta;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Apuesta apuesta = (Apuesta) o;
        return id == apuesta.id &&
                cantidad == apuesta.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @ManyToOne
    @JoinColumn(name = "id_evento", referencedColumnName = "id", nullable = false)
    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    @ManyToOne
    @JoinColumn(name = "id_apuesta", referencedColumnName = "id", nullable = false)
    public Participante getApuesta() {
        return apuesta;
    }

    public void setApuesta(Participante apuesta) {
        this.apuesta = apuesta;
    }

    @Basic
    public Byte getTerminada() {
        return terminada;
    }

    public void setTerminada(Byte terminada) {
        this.terminada = terminada;
    }

    @Basic
    public Byte getGanada() {
        return ganada;
    }

    public void setGanada(Byte ganada) {
        this.ganada = ganada;
    }
}
