package com.jaimepardo.mapaDB;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Participantecampeonato implements Serializable {
    private int id;
    private Integer victorias;
    private Integer empates;
    private Integer derrotas;
    private Integer puntos;
    private Participante participante;
    private Campeonato campeonato;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "victorias")
    public Integer getVictorias() {
        return victorias;
    }

    public void setVictorias(Integer victorias) {
        this.victorias = victorias;
    }

    @Basic
    @Column(name = "empates")
    public Integer getEmpates() {
        return empates;
    }

    public void setEmpates(Integer empates) {
        this.empates = empates;
    }

    @Basic
    @Column(name = "derrotas")
    public Integer getDerrotas() {
        return derrotas;
    }

    public void setDerrotas(Integer derrotas) {
        this.derrotas = derrotas;
    }

    @Basic
    @Column(name = "puntos")
    public Integer getPuntos() {
        return puntos;
    }

    public void setPuntos(Integer puntos) {
        this.puntos = puntos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participantecampeonato that = (Participantecampeonato) o;
        return id == that.id &&
                Objects.equals(victorias, that.victorias) &&
                Objects.equals(empates, that.empates) &&
                Objects.equals(derrotas, that.derrotas) &&
                Objects.equals(puntos, that.puntos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, victorias, empates, derrotas, puntos);
    }

    @ManyToOne
    @JoinColumn(name = "id_participante", referencedColumnName = "id")
    public Participante getParticipante() {
        return participante;
    }

    public void setParticipante(Participante participante) {
        this.participante = participante;
    }

    @ManyToOne
    @JoinColumn(name = "id_campeonato", referencedColumnName = "id")
    public Campeonato getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(Campeonato campeonato) {
        this.campeonato = campeonato;
    }

    @Override
    public String toString() {
        return participante.toString();
    }
}
