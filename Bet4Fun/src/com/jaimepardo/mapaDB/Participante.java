package com.jaimepardo.mapaDB;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
public class Participante implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private String nombre;
    private Integer victorias;
    private Integer empates;
    private Integer derrotas;
    private String logo;
    private Deporte deporte;
    private List<Participantecampeonato> campeonato;
    private List<Participanteevento> eventos;
    private List<Apuesta> apuestas;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "victorias")
    public Integer getVictorias() {
        return victorias;
    }

    public void setVictorias(Integer victorias) {
        this.victorias = victorias;
    }

    @Basic
    @Column(name = "empates")
    public Integer getEmpates() {
        return empates;
    }

    public void setEmpates(Integer empates) {
        this.empates = empates;
    }

    @Basic
    @Column(name = "derrotas")
    public Integer getDerrotas() {
        return derrotas;
    }

    public void setDerrotas(Integer derrotas) {
        this.derrotas = derrotas;
    }

    @Basic
    @Column(name = "logo")
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participante that = (Participante) o;
        return id == that.id &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(victorias, that.victorias) &&
                Objects.equals(empates, that.empates) &&
                Objects.equals(derrotas, that.derrotas) &&
                Objects.equals(logo, that.logo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, victorias, empates, derrotas, logo);
    }

    @ManyToOne
    @JoinColumn(name = "id_deporte", referencedColumnName = "id")
    public Deporte getDeporte() {
        return deporte;
    }

    public void setDeporte(Deporte deporte) {
        this.deporte = deporte;
    }

    @OneToMany(mappedBy = "participante")
    public List<Participantecampeonato> getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(List<Participantecampeonato> campeonato) {
        this.campeonato = campeonato;
    }

    @OneToMany(mappedBy = "participante")
    public List<Participanteevento> getEventos() {
        return eventos;
    }

    public void setEventos(List<Participanteevento> eventos) {
        this.eventos = eventos;
    }

    @OneToMany(mappedBy = "apuesta")
    public List<Apuesta> getApuestas() {
        return apuestas;
    }

    public void setApuestas(List<Apuesta> apuestas) {
        this.apuestas = apuestas;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
