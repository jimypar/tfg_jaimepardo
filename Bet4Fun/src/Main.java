import gui.Login;

import javax.swing.*;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        tema();
        Login vista = new Login();

    }

    /**
     * Metodo que asigna el tema y los colores de la Vista.
     */
    public static void tema() {
        UIManager.put( "control", new Color(101, 101, 101, 255) );
        //UIManager.put( "info", new Color(103, 70,128) );
        UIManager.put( "nimbusBase", new Color(0, 44, 59) );
        UIManager.put( "nimbusLightBackground", new Color(64, 64, 64) );
        //UIManager.put( "nimbusSelectedText", new Color( 255, 255, 255) );
        UIManager.put( "nimbusSelectionBackground", new Color(71, 104, 156) );
        UIManager.put( "text", new Color(255, 255, 255) );
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }

    }
}