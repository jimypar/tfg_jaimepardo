package com.jaimepardo.mapaDB;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
public class Evento implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private LocalDateTime fecha;
    private Byte terminado;
    private List<Apuesta> apuestas;
    private Campeonato campeonato;
    private List<Participanteevento> participantes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha")
    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "terminado")
    public Byte getTerminado() {
        return terminado;
    }

    public void setTerminado(Byte terminado) {
        this.terminado = terminado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Evento evento = (Evento) o;
        return id == evento.id &&
                Objects.equals(fecha, evento.fecha) &&
                Objects.equals(terminado, evento.terminado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fecha, terminado);
    }

    @OneToMany(mappedBy = "evento")
    public List<Apuesta> getApuestas() {
        return apuestas;
    }

    public void setApuestas(List<Apuesta> apuestas) {
        this.apuestas = apuestas;
    }

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_campeonato", referencedColumnName = "id", nullable = false)
    public Campeonato getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(Campeonato campeonato) {
        this.campeonato = campeonato;
    }

    @OneToMany(mappedBy = "evento", fetch = FetchType.EAGER)
    public List<Participanteevento> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<Participanteevento> participantes) {
        this.participantes = participantes;
    }

    @Override
    public String toString() {
        return campeonato + " : " + id;
    }
}
