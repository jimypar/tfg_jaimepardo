package com.jaimepardo.mapaDB;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Participanteevento implements Serializable {
    private int id;
    private Integer resultado;
    private Double cuota;
    private Byte empate;
    private Byte victoria;
    private Evento evento;
    private Participante participante;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "resultado")
    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }

    @Basic
    @Column(name = "empate")
    public Byte getEmpate() {
        return empate;
    }

    public void setEmpate(Byte empate) {
        this.empate = empate;
    }

    @Basic
    @Column(name = "cuota")
    public Double getCuota() {
        return cuota;
    }

    public void setCuota(Double cuota) {
        this.cuota = cuota;
    }

    @Basic
    @Column(name = "victoria")
    public Byte getVictoria() {
        return victoria;
    }

    public void setVictoria(Byte victoria) {
        this.victoria = victoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participanteevento that = (Participanteevento) o;
        return id == that.id &&
                Objects.equals(resultado, that.resultado) &&
                Objects.equals(empate, that.empate) &&
                Objects.equals(victoria, that.victoria);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, resultado, empate, victoria);
    }

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_evento", referencedColumnName = "id")
    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_participante", referencedColumnName = "id")
    public Participante getParticipante() {
        return participante;
    }

    public void setParticipante(Participante participante) {
        this.participante = participante;
    }

    @Override
    public String toString() {
        return participante.toString();
    }

}
