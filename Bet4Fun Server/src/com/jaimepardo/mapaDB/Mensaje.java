package com.jaimepardo.mapaDB;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class Mensaje implements Serializable {

    private Usuario usuario;
    private String consulta;
    private int idConsulta;
    private LocalDateTime diaConsulta;
    private Boolean comprobacion;
    private Object objeto;
    private List<Object> listaObjetos;

    public Mensaje(Usuario sessionUser) {
        this.usuario = sessionUser;
    }

    public Mensaje() {

    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public Boolean getComprobacion() {
        return comprobacion;
    }

    public void setComprobacion(Boolean comprobacion) {
        this.comprobacion = comprobacion;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public List<Object> getListaObjetos() {
        return listaObjetos;
    }

    public void setListaObjetos(List<Object> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    public int getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(int idConsulta) {
        this.idConsulta = idConsulta;
    }

    public LocalDateTime getDiaConsulta() {
        return diaConsulta;
    }

    public void setDiaConsulta(LocalDateTime diaConsulta) {
        this.diaConsulta = diaConsulta;
    }

    @Override
    public String toString() {
        return "Mensaje{" +
                "usuario=" + usuario +
                ", consulta='" + consulta + '\'' +
                ", comprobacion=" + comprobacion +
                ", objeto=" + objeto +
                ", listaObjetos=" + listaObjetos +
                '}';
    }
}
