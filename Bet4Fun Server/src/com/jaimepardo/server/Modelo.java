package com.jaimepardo.server;

import com.jaimepardo.mapaDB.Mensaje;
import com.jaimepardo.mapaDB.*;
import com.jaimepardo.util.HibernateUtil;
import hash.Hashear;
import org.hibernate.Session;
import org.hibernate.query.Query;
import rsa.EncriptadoRSA;

import javax.persistence.PersistenceException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Modelo {


    /**
     * Metodo que conecta con la base de datos
     *
     */
    void conectar() {

        HibernateUtil.buildSessionFactory();
        HibernateUtil.openSession();

    }

    /**
     * Metodo que desconecta la base
     */
    void desconectar() {
        HibernateUtil.closeSessionFactory();
    }


    /**
     * Metodo que inserta un Usuario
     */
    Usuario insertarUsuario(Usuario usuario) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        Usuario usuarioreturn = (Usuario) sesion.merge(usuario);
        sesion.getTransaction().commit();
        sesion.close();
        return usuarioreturn;

    }

    /**
     * Metodo que inserta una Apuesta
     * @return
     */
    Usuario insertarApuesta(Apuesta apuesta) {

        //apuesta.setCantidad(cant);
        Usuario user = apuesta.getUsuario();
        user.setPuntos(user.getPuntos()-apuesta.getCantidad());

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.merge(apuesta);
        Usuario usuario = (Usuario) sesion.merge(user);
        sesion.getTransaction().commit();
        sesion.close();

        return usuario;

    }

    /**
     * Metodo que inserta un Evento
     */
    public Evento insertarEvento(Evento evento) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(evento);
        sesion.getTransaction().commit();
        sesion.close();
        return evento;

    }

    public Participanteevento insertarParticipanteEvento(Participanteevento participanteevento) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        Participanteevento p = (Participanteevento) sesion.merge(participanteevento);
        sesion.getTransaction().commit();
        sesion.close();
        return p;

    }

    /**
     * Metodo que inserta un Participante
     */
    void insertarParticipante(Participante participante) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.merge(participante);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta un Campeonato
     */
    void insertarCampeonato(Campeonato campeonato) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.merge(campeonato);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta un participante a un campeonato
     */
    public void insertarParticipanteCampeonato(Participantecampeonato participante) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.merge(participante);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta un Deporte
     */
    void insertarDeporte(Deporte deporte) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.merge(deporte);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta un Rol
     */
    void insertarRol(Rol rol) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.merge(rol);
        sesion.getTransaction().commit();
        sesion.close();

    }

    public void actualizarParticipantes(List<Participanteevento> lista) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        for (Participanteevento participante : lista) {
            sesion.merge(participante);
        }
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que borra un Usuario
     */
    public void borrarUsuario(Usuario usuario) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(usuario);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que borra una Apuesta
     */
    void borrarApuesta(Apuesta apuesta) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(apuesta);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un Evento
     */
    void borrarEvento(Evento evento) {

        HibernateUtil.openSession();
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(evento);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un Participante
     */
    void borrarParticipante(Participante participante) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(participante);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un Campeonato
     */
    void borrarCampeonato(Campeonato campeonato) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(campeonato);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un Deporte
     */
    void borrarDeporte(Deporte deporte) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(deporte);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un Rol
     */
    void borrarRol(Rol rol) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(rol);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un Participante de un campeonato
     */
    public void borrarParticipantecampeonato(Participantecampeonato participantecampeonato) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(participantecampeonato);
        sesion.getTransaction().commit();
        sesion.close();

    }


    public void borrarParticipantesEvento(Evento evento) {

        List<Participanteevento> participantesEvento = getParticipanteseventoEvento(evento.getId());
        for (Participanteevento participanteEvento: participantesEvento) {
            borrarParticipanteEvento(participanteEvento);
        }

    }

    private void borrarParticipanteEvento(Participanteevento participanteEvento) {

        HibernateUtil.openSession();
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.flush();
        sesion.clear();
        sesion.delete(participanteEvento);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que busca un Usuario
     */
    public Usuario getUsuario(int idUsuario) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE id = :id");
        query.setParameter("id", idUsuario);
        return (Usuario) query.getSingleResult();
    }

    /**
     * Metodo que busca una Apuesta
     */
    public Apuesta getApuesta(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Apuesta WHERE id = :id");
        query.setParameter("id", id);
        return (Apuesta) query.getSingleResult();
    }

    /**
     * Metodo que busca un Evento
     */
    public Evento getEvento(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Evento WHERE id = :id");
        query.setParameter("id", id);
        return (Evento) query.getSingleResult();
    }


    /**
     * Metodo que busca un Participante
     */
    public Participante getParticipante(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Participante WHERE id = :id");
        query.setParameter("id", id);
        return (Participante) query.getSingleResult();
    }

    /**
     * Metodo que busca un Campeonato
     */
    public Campeonato getCampeonato(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Campeonato WHERE id = :id");
        query.setParameter("id", id);
        return (Campeonato) query.getSingleResult();
    }

    /**
     * Metodo que busca un Deporte
     */
    public Deporte getDeporte(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Deporte WHERE id = :id");
        query.setParameter("id", id);
        return (Deporte) query.getSingleResult();
    }

    /**
     * Metodo que busca un Rol
     */
    public Rol getRol(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Rol WHERE id = :id");
        query.setParameter("id", id);
        return (Rol) query.getSingleResult();
    }

    public Participantecampeonato getParticipantecampeonato(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Participantecampeonato WHERE id = :id");
        query.setParameter("id", id);
        return (Participantecampeonato) query.getSingleResult();
    }

    public List<Usuario> getUsuarios() {
        return (List<Usuario>) HibernateUtil.getCurrentSession().createQuery("FROM Usuario").getResultList();
    }

    public List<Apuesta> getApuestas(Object obj) {
        Usuario usuario = (Usuario) obj;
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Apuesta WHERE usuario = :usuario");
        query.setParameter("usuario", usuario);
        return query.getResultList();
    }

    public List<Evento> getEventos() {
        return (List<Evento>) HibernateUtil.getCurrentSession().createQuery("FROM Evento ").getResultList();
    }

    public List<Participante> getParticipantes() {
        return (List<Participante>) HibernateUtil.getCurrentSession().createQuery("FROM Participante").getResultList();
    }

    public List<Campeonato> getCampeonatos() {
        return (List<Campeonato>) HibernateUtil.getCurrentSession().createQuery("FROM Campeonato ").getResultList();
    }

    public List<Deporte> getDeportes() {
        return (List<Deporte>) HibernateUtil.getCurrentSession().createQuery("FROM Deporte ").getResultList();
    }

    public List<Rol> getRoles() {
        return (List<Rol>) HibernateUtil.getCurrentSession().createQuery("FROM Rol").getResultList();
    }

    public List<Participantecampeonato> getParticipantesCampeonato(Campeonato campeonato) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Participantecampeonato WHERE campeonato = :campeonato");
        query.setParameter("campeonato", campeonato);
        return query.getResultList();
    }

    public List<Participanteevento> getParticipanteseventoEvento(int idEvento) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Participanteevento WHERE evento.id = :idevento");
        query.setParameter("idevento", idEvento);
        return query.getResultList();
    }

    public List<Participante> getParticipantesCampeonatoEvento(Campeonato campeonato) {
        List<Participante> participantescampeonato = new ArrayList<>();
        for (Participantecampeonato participantecampeonato: getParticipantesCampeonato(campeonato)) {
            participantescampeonato.add(participantecampeonato.getParticipante());
        }
        return participantescampeonato;
    }

    public List<Participante> getParticipantesEvento(int idEvento) {
        List<Participante> participantesEvento = new ArrayList<>();
        for (Participanteevento participanteevento: getParticipanteseventoEvento(idEvento)) {
            participantesEvento.add(participanteevento.getParticipante());
        }
        return participantesEvento;
    }

    public List<Participante> getParticipantesDeporte(Deporte deporte) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Participante WHERE deporte = :deporte");
        query.setParameter("deporte", deporte);
        return query.getResultList();
    }


    public List<Evento> getEventosDia(LocalDateTime day) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Evento WHERE DATE(fecha) = DATE(:day)");
        query.setParameter("day", day);
        return query.getResultList();
    }

    public List<Apuesta> getApuestasUsuario(int id) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Apuesta WHERE usuario.id = :id ORDER BY id DESC");
        query.setParameter("id", id);
        return query.getResultList();
    }

    public List<Evento> getEventosTerminados() {
        return (List<Evento>) HibernateUtil.getCurrentSession().createQuery("FROM Evento WHERE terminado = 1").getResultList();
    }

    public Object getRanking() {
        return (List<Usuario>) HibernateUtil.getCurrentSession().createQuery("FROM Usuario ORDER BY puntos DESC").getResultList();
    }


    //LOGIN-----------------------

    public Mensaje iniciarSesion(Mensaje mensaje) {

        Usuario user;
        try {
            Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE username = :username");
            query.setParameter("username", mensaje.getUsuario().getUsername());
            user = (Usuario) query.getSingleResult();
        }catch (Exception e){
            mensaje.setComprobacion(false);
            return mensaje;
        }


        if (user.getPass().equals(mensaje.getUsuario().getPass())){
            mensaje.setComprobacion(true);
            mensaje.setUsuario(user);
            return mensaje;
        }

        mensaje.setComprobacion(false);
        return mensaje;

    }

    public Mensaje registrarUsuario(Mensaje mensaje) {

        Usuario usuario = mensaje.getUsuario();

        try {
            Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE username = :username");
            query.setParameter("username", usuario.getUsername());
            Usuario user = (Usuario) query.getSingleResult();
        }catch (PersistenceException e){
            usuario.setPuntos(1000);
            usuario.setRol(getRol(3));
            insertarUsuario(mensaje.getUsuario());
            mensaje.setComprobacion(true);
            return mensaje;
        }

        mensaje.setComprobacion(false);
        return mensaje;

    }

    public void terminarEventos() {

        LocalDateTime now = LocalDateTime.now();

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Evento WHERE terminado = 0 AND fecha <= :now");
        query.setParameter("now", now);
        List<Evento> listaEventos = query.getResultList();

        if (listaEventos!=null){

            Session sesion = HibernateUtil.getCurrentSession();
            sesion.beginTransaction();

            for (Evento e: listaEventos) {
                e.setTerminado((byte) 1);
                sesion.merge(e);
            }

            sesion.getTransaction().commit();
            sesion.close();

        }
    }

    public void cerrarApuestas() {

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Apuesta WHERE terminada is null AND evento.terminado = 1");
        List<Apuesta> listaApuestas = query.getResultList();

        if (listaApuestas!=null){

            Session sesion = HibernateUtil.getCurrentSession();
            sesion.beginTransaction();

            for (Apuesta a: listaApuestas) {
                Evento e = a.getEvento();
                Usuario u = a.getUsuario();
                for (Participanteevento p: e.getParticipantes()) {
                    if (p.getParticipante().equals(a.getApuesta()) && p.getVictoria()!=null){
                        if (p.getVictoria() == 1){
                            a.setGanada((byte) 1);
                            u.setPuntos((int) (u.getPuntos()+(a.getCantidad()*p.getCuota())));
                        }else {
                            a.setGanada((byte) 0);
                        }
                        a.setTerminada((byte) 1);
                    }
                }

                sesion.merge(u);
                sesion.merge(a);
            }

            sesion.getTransaction().commit();
            sesion.close();

        }

    }


}
