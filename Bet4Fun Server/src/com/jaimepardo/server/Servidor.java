package com.jaimepardo.server;

import com.jaimepardo.mapaDB.Mensaje;
import com.jaimepardo.mapaDB.Usuario;
import com.jaimepardo.server.gui.Controlador;
import com.jaimepardo.server.gui.Vista;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Servidor {

    private Modelo modelo;
    private ControladorServer controlador;
    private Controlador c;
    private static int idUsuario;
    private ArrayList<HiloCliente> clientes;
    private int puerto;
    private boolean continuar;
    public boolean online;

    //Metodo principal de la clase que inicia la base de datos que
    // crea el servidor con el puerto correspondiente y lo inicia
    public static void main(String[] args) {
        Servidor servidor = new Servidor(4444);
        servidor.start();
    }

    //Constructor del servidor que le asigna el puerto y la base de datos e inicia el array de clientes.
    public Servidor(int puerto) {
        this.c = new Controlador(new Vista());
        this.puerto = puerto;
        clientes = new ArrayList<HiloCliente>();
        modelo = new Modelo();
        controlador = new ControladorServer(modelo);
        online = true;
    }

    //Metodo que inicia el servidor crea la conexion con la base de datos
    //Inicia el socket del server y constantemente busca conexiones de clientes
    //acepta la conexion, la añade al array y crea el hilo.
    public void start() {

        modelo.conectar();

        HiloAutomatico hiloActualizar = new HiloAutomatico();
        hiloActualizar.start();


        continuar = true;
        try
        {
            ServerSocket serverSocket = new ServerSocket(puerto);
            while(continuar){
                Socket socket = serverSocket.accept();

                if(!continuar){
                    break;
                }

                HiloCliente hiloCliente = new HiloCliente(socket);
                clientes.add(hiloCliente);
                hiloCliente.start();
            }

            serverSocket.close();
            for(int i = 0; i < clientes.size(); ++i) {
                HiloCliente cliente = clientes.get(i);
                    cliente.entrada.close();
                    cliente.salida.close();
                    cliente.socket.close();
            }
        }
        catch (IOException e) {
            System.out.println("Error al crear Servidor");
        }
    }

    //Hilo del cliente que contiene el socket, la entrada, la salida y un ID.
    class HiloCliente extends Thread {
        Socket socket;
        ObjectInputStream entrada;
        ObjectOutputStream salida;

        int id;
        boolean conectado;

        //Constructor del hilo del cliente que inicia el socket le asigna una id
        //y inicia la salida y entrada
        HiloCliente(Socket socket) {
            Usuario user;
            id = ++idUsuario;
            this.socket = socket;
            try
            {
                salida = new ObjectOutputStream(socket.getOutputStream());
                entrada = new ObjectInputStream(socket.getInputStream());
            }
            catch (IOException e) {

            }
        }

        //El hilo recibe mensajes del cliente y
        //Si NO esta conectado:
        //  -Comprueba el mensaje si es el usuario y contraseña correctos
        //  -Le devuelve los permisos que tiene es usuario.
        //  Si esta conectado:
        //  -Recibe un mensaje y realiza la consulta el la base de datos.
        public void run() {

            boolean continuar = true;
            Mensaje mensaje = new Mensaje();

            while(continuar) {

                try {
                    mensaje = (Mensaje) entrada.readObject();
                } catch (SocketException s){
                    continuar = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (conectado){

                    if (mensaje.getConsulta().equals("desconectarUsuario")) {
                        continuar = false;
                        continue;
                    }

                    Mensaje resultado = controlador.comprobar(mensaje);
                    c.mostrar(resultado);

                    System.out.println(resultado);
                    try {
                        salida.writeObject(resultado);
                    } catch (Exception e) {
                    }



                }else {
                    Mensaje envio = controlador.conectarUsuario(mensaje);

                    if (envio.getConsulta().equals("login")){
                        conectado=envio.getComprobacion();

                    }


                    if (envio.getConsulta().equals("desconectarUsuario")) {
                        continuar = false;
                        continue;
                    }
                    System.out.println(envio);
                    try {
                        salida.writeObject(envio);
                    } catch (Exception e) {
                        System.out.println("Error al envio del mensaje");
                    }
                }
            }
            cerrar();
        }


        //Cierra la conexion del usuario
        private void cerrar() {
            try {
                salida.close();
                entrada.close();
                socket.close();
                System.out.println("\n"+LocalDateTime.now()+" - Usuario "+idUsuario+" desconectado");
                c.vista.txtLog.append("\n"+LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))+" - Usuario "+idUsuario+" desconectado");
                c.vista.txtLog.setCaretPosition(c.vista.txtLog.getDocument().getLength());

            }
            catch (Exception e) {}
        }

    }



    class HiloAutomatico extends Thread {

             public void run(){

                 while (true) {
                     controlador.actualizar();
                     c.vista.txtLog.append("\n"+LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))+ " - Actualizado");
                     c.vista.txtLog.setCaretPosition(c.vista.txtLog.getDocument().getLength());
                     System.out.println("\n"+LocalDateTime.now()+ " - Actualizado");
                     try {
                         this.sleep(600000);
                     } catch (InterruptedException e) {
                         e.printStackTrace();
                     }
                 }

             }

    }

}

