package com.jaimepardo.server;

import com.jaimepardo.mapaDB.Mensaje;
import com.jaimepardo.mapaDB.*;

import java.util.List;

public class ControladorServer {


    private Modelo modelo;

    public ControladorServer(Modelo modelo) {
        this.modelo = modelo;
    }

    public Mensaje conectarUsuario(Mensaje mensaje) {

        switch (mensaje.getConsulta()){
            case "login":
                return modelo.iniciarSesion(mensaje);
            case "register":
                return modelo.registrarUsuario(mensaje);
        }

        return mensaje;

    }

    public Mensaje comprobar(Mensaje mensaje) {

        try {

            switch (mensaje.getConsulta()) {
                case "insertarUsuario":
                    modelo.insertarUsuario((Usuario) mensaje.getObjeto());
                    break;
                case "insertarApuesta":
                    mensaje.setUsuario(modelo.insertarApuesta((Apuesta) mensaje.getObjeto()));
                    break;
                case "insertarEvento":
                    mensaje.setObjeto(modelo.insertarEvento((Evento) mensaje.getObjeto()));
                    break;
                case "insertarParticipanteEvento":
                    modelo.insertarParticipanteEvento((Participanteevento) mensaje.getObjeto());
                    break;
                case "insertarParticipante":
                    modelo.insertarParticipante((Participante) mensaje.getObjeto());
                    break;
                case "insertarCampeonato":
                    modelo.insertarCampeonato((Campeonato) mensaje.getObjeto());
                    break;
                case "insertarParticipanteCampeonato":
                    modelo.insertarParticipanteCampeonato((Participantecampeonato) mensaje.getObjeto());
                    break;
                case "insertarDeporte":
                    modelo.insertarDeporte((Deporte) mensaje.getObjeto());
                    break;
                case "insertarRol":
                    modelo.insertarRol((Rol) mensaje.getObjeto());
                    break;
                case "actualizarParticipantes":
                    modelo.actualizarParticipantes((List<Participanteevento>) (Object) mensaje.getListaObjetos());
                    break;
                case "borrarUsuario":
                    modelo.borrarUsuario((Usuario) mensaje.getObjeto());
                    break;
                case "borrarApuesta":
                    modelo.borrarApuesta((Apuesta) mensaje.getObjeto());
                    break;
                case "borrarEvento":
                    modelo.borrarEvento((Evento) mensaje.getObjeto());
                    break;
                case "borrarParticipante":
                    modelo.borrarParticipante((Participante) mensaje.getObjeto());
                    break;
                case "borrarCampeonato":
                    modelo.borrarCampeonato((Campeonato) mensaje.getObjeto());
                    break;
                case "borrarDeporte":
                    modelo.borrarDeporte((Deporte) mensaje.getObjeto());
                    break;
                case "borrarRol":
                    modelo.borrarRol((Rol) mensaje.getObjeto());
                    break;
                case "borrarParticipantecampeonato":
                    modelo.borrarParticipantecampeonato((Participantecampeonato) mensaje.getObjeto());
                    break;
                case "borrarParticipantesEvento":
                    modelo.borrarParticipantesEvento((Evento) mensaje.getObjeto());
                    break;
                case "getUsuario":
                    mensaje.setObjeto(modelo.getUsuario(mensaje.getIdConsulta()));
                    break;
                case "getApuesta":
                    mensaje.setObjeto(modelo.getApuesta(mensaje.getIdConsulta()));
                    break;
                case "getEvento":
                    mensaje.setObjeto(modelo.getEvento(mensaje.getIdConsulta()));
                    break;
                case "getParticipante":
                    mensaje.setObjeto(modelo.getParticipante(mensaje.getIdConsulta()));
                    break;
                case "getCampeonato":
                    mensaje.setObjeto(modelo.getCampeonato(mensaje.getIdConsulta()));
                    break;
                case "getDeporte":
                    mensaje.setObjeto(modelo.getDeporte(mensaje.getIdConsulta()));
                    break;
                case "getRol":
                    mensaje.setObjeto(modelo.getRol(mensaje.getIdConsulta()));
                    break;
                case "getUsuarios":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getUsuarios());
                    break;
                case "getApuestas":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getApuestas(mensaje.getObjeto()));
                    break;
                case "getEventos":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getEventos());
                    break;
                case "getParticipantes":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getParticipantes());
                    break;
                case "getCampeonatos":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getCampeonatos());
                    break;
                case "getDeportes":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getDeportes());
                    break;
                case "getRoles":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getRoles());
                    break;
                case "getParticipantesCampeonato":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getParticipantesCampeonato((Campeonato) mensaje.getObjeto()));
                    break;
                case "getParticipanteseventoEvento":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getParticipanteseventoEvento(mensaje.getIdConsulta()));
                    break;
                case "getParticipantesCampeonatoEvento":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getParticipantesCampeonatoEvento((Campeonato) mensaje.getObjeto()));
                    break;
                case "getParticipantesEvento":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getParticipantesEvento(mensaje.getIdConsulta()));
                    break;
                case "getParticipantesDeporte":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getParticipantesDeporte((Deporte) mensaje.getObjeto()));
                    break;
                case "getEventosDia":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getEventosDia(mensaje.getDiaConsulta()));
                    break;
                case "getApuestasUsuario":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getApuestasUsuario(mensaje.getIdConsulta()));
                    break;
                case "getEventosTerminados":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getEventosTerminados());
                    break;
                case "getRankingUsuarios":
                    mensaje.setListaObjetos((List<Object>) (Object) modelo.getRanking());
                    break;
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return mensaje;

    }

    public void actualizar() {

        modelo.terminarEventos();

        modelo.cerrarApuestas();

    }
}
