package com.jaimepardo.server.gui;
import com.jaimepardo.mapaDB.Mensaje;

import javax.swing.*;
import java.awt.event.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Jaime Pardo on 13/12/2022.
 */
public class Controlador implements ActionListener {
    public Vista vista;
    boolean refrescar;

    public Controlador(Vista vista) {
        this.vista = vista;
        addActionListeners(this);
    }

    private void addActionListeners(ActionListener listener) {

        vista.informeUsuarios.addActionListener(this);
        vista.informeUsuarios.setActionCommand("informeUsuarios");
        vista.informeApuestasUsuarios.addActionListener(this);
        vista.informeApuestasUsuarios.setActionCommand("informeApuestasUsuarios");
        vista.informeParticipantes.addActionListener(this);
        vista.informeParticipantes.setActionCommand("informeParticipantes");
        vista.informeApuestasDeUsuario.addActionListener(this);
        vista.informeApuestasDeUsuario.setActionCommand("informeApuestasDeUsuario");
        vista.graficoApuestasDeporte.addActionListener(this);
        vista.graficoApuestasDeporte.setActionCommand("graficoApuestasDeporte");
        vista.graficoPuntosDeporte.addActionListener(this);
        vista.graficoPuntosDeporte.setActionCommand("graficoPuntosDeporte");

    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */


    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "informeUsuarios":
                JasperReport.generar("InformeUsuarios");
                break;
            case "informeApuestasUsuarios":
                JasperReport.generar("InformeApuestasUsuario");
                break;
            case "informeParticipantes":
                JasperReport.generar("InformeParticipantes");
                break;
            case "informeApuestasDeUsuario":
                JTextField usuario = new JTextField(10);
                String[] options = new String[]{"OK", "Cancel"};
                int option = JOptionPane.showOptionDialog(null, usuario, "Introduce un usuario",
                        JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                        null, options, options[1]);
                if (option == 0) {
                    JasperReport.generarBusquedaNombre(usuario.getText());
                }
                break;
            case "graficoApuestasDeporte":
                JasperReport.generar("GraficoApuestas");
                break;
            case "graficoPuntosDeporte":
                JasperReport.generar("GraficoPuntos");
                break;
        }
    }

    public void mostrar(Mensaje m) {
        vista.txtLog.append("\n" + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")) + " - Usuario: " + m.getUsuario() + ", Consulta: " + m.getConsulta());
        vista.txtLog.setCaretPosition(vista.txtLog.getDocument().getLength());
    }

}