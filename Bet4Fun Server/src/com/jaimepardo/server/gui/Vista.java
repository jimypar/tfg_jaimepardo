package com.jaimepardo.server.gui;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JTextArea txtLog;
    public JButton informeUsuarios;
    public JButton informeApuestasUsuarios;
    public JButton informeParticipantes;
    public JButton informeApuestasDeUsuario;
    public JButton graficoApuestasDeporte;
    public JButton graficoPuntosDeporte;

    public Vista() {
        super("Servidor");
        initFrame();
    }

    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+300,this.getHeight()+400));
        this.setLocationRelativeTo(this);
    }

}
