package com.jaimepardo.server;

import com.jaimepardo.mapaDB.Mensaje;
import com.jaimepardo.mapaDB.*;

import java.time.LocalDateTime;
import java.util.List;
import com.jaimepardo.util.Util;

public class ControladorCliente {

    public Usuario sessionUser;
    public boolean online;
    Mensaje mensaje;
    Cliente cliente;
    private boolean terminado;


    public ControladorCliente() {
        this.cliente = new Cliente("localhost", 4444, this);
        cliente.iniciar();
        this.sessionUser = new Usuario();
        this.online = true;
    }

    public void desconectar() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("desconectarUsuario");
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que inserta un Usuario
     */
    public void insertarUsuario(Usuario usuario) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarUsuario");
        mensaje.setObjeto(usuario);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que inserta una Apuesta
     */
    public void insertarApuesta(Apuesta apuesta) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarApuesta");
        mensaje.setObjeto(apuesta);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        this.sessionUser = cliente.respuesta.getUsuario();
        return;

    }

    /**
     * Metodo que modifica una Apuesta
     */
    public void modificarApuesta(Apuesta apuesta) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarApuesta");
        mensaje.setObjeto(apuesta);
        cliente.enviarMensaje(mensaje);

    }




    /**
     * Metodo que inserta un Evento
     */
    public Evento insertarEvento(Evento evento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarEvento");
        mensaje.setObjeto(evento);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Evento) cliente.respuesta.getObjeto();

    }

    public void insertarParticipanteEvento(Participanteevento participanteevento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarParticipanteEvento");
        mensaje.setObjeto(participanteevento);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return;

    }

    /**
     * Metodo que inserta un Participante
     */
    public void insertarParticipante(Participante participante) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarParticipante");
        mensaje.setObjeto(participante);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que inserta un Campeonato
     */
    public void insertarCampeonato(Campeonato campeonato) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarCampeonato");
        mensaje.setObjeto(campeonato);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que inserta un participante a un campeonato
     */
    public void insertarParticipanteCampeonato(Participantecampeonato participante) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarParticipanteCampeonato");
        mensaje.setObjeto(participante);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que inserta un Deporte
     */
    public void insertarDeporte(Deporte deporte) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarDeporte");
        mensaje.setObjeto(deporte);
        cliente.enviarMensaje(mensaje);


    }

    /**
     * Metodo que inserta un Rol
     */
    public void insertarRol(Rol rol) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("insertarRol");
        mensaje.setObjeto(rol);
        cliente.enviarMensaje(mensaje);

    }

    public void actualizarParticipantes(List<Participanteevento> lista) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("actualizarParticipantes");
        mensaje.setListaObjetos((List<Object>)(Object) lista);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Usuario
     */
    public void borrarUsuario(Usuario usuario) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarUsuario");
        mensaje.setObjeto(usuario);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra una Apuesta
     */
    public void borrarApuesta(Apuesta apuesta) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarApuesta");
        mensaje.setObjeto(apuesta);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Evento
     */
    public void borrarEvento(Evento evento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarEvento");
        mensaje.setObjeto(evento);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Participante
     */
    public void borrarParticipante(Participante participante) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarParticipante");
        mensaje.setObjeto(participante);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Campeonato
     */
    public void borrarCampeonato(Campeonato campeonato) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarCampeonato");
        mensaje.setObjeto(campeonato);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Deporte
     */
    public void borrarDeporte(Deporte deporte) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarDeporte");
        mensaje.setObjeto(deporte);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Rol
     */
    public void borrarRol(Rol rol) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarRol");
        mensaje.setObjeto(rol);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que borra un Participante de un campeonato
     */
    public void borrarParticipantecampeonato(Participantecampeonato participantecampeonato) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarParticipantecampeonato");
        mensaje.setObjeto(participantecampeonato);
        cliente.enviarMensaje(mensaje);

    }


    public void borrarParticipantesEvento(Evento evento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarParticipantesEvento");
        mensaje.setObjeto(evento);
        cliente.enviarMensaje(mensaje);

    }

    private void borrarParticipanteEvento(Participanteevento participanteEvento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("borrarParticipanteEvento");
        mensaje.setObjeto(participanteEvento);
        cliente.enviarMensaje(mensaje);

    }

    /**
     * Metodo que busca un Usuario
     */
    public Usuario getUsuario(int idUsuario) {

        Mensaje mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getUsuario");
        mensaje.setIdConsulta(idUsuario);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Usuario) cliente.respuesta.getObjeto();

    }

    /**
     * Metodo que busca una Apuesta
     */
    public Apuesta getApuesta(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getApuesta");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Apuesta)cliente.respuesta.getObjeto();

    }

    /**
     * Metodo que busca un Evento
     */
    public Evento getEvento(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getEvento");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Evento)cliente.respuesta.getObjeto();

    }


    /**
     * Metodo que busca un Participante
     */
    public Participante getParticipante(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipante");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Participante)cliente.respuesta.getObjeto();

    }

    /**
     * Metodo que busca un Campeonato
     */
    public Campeonato getCampeonato(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getCampeonato");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Campeonato)cliente.respuesta.getObjeto();

    }

    /**
     * Metodo que busca un Deporte
     */
    public Deporte getDeporte(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getDeporte");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Deporte)cliente.respuesta.getObjeto();

    }

    /**
     * Metodo que busca un Rol
     */
    public Rol getRol(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getRol");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Rol)cliente.respuesta.getObjeto();

    }

    public Participantecampeonato getParticipantecampeonato(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipantecampeonato");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (Participantecampeonato)cliente.respuesta.getObjeto();

    }

    public List<Usuario> getUsuarios() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getUsuarios");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Usuario>)(Object)cliente.respuesta.getListaObjetos();


    }

    public List<Apuesta> getApuestas(Usuario user) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getApuestas");
        mensaje.setObjeto(user);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Apuesta>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Evento> getEventos() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getEventos");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Evento>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Participante> getParticipantes() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipantes");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Participante>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Campeonato> getCampeonatos() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getCampeonatos");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Campeonato>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Deporte> getDeportes() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getDeportes");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Deporte>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Rol> getRoles() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getRoles");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Rol>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Participantecampeonato> getParticipantesCampeonato(Campeonato campeonato) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipantesCampeonato");
        mensaje.setObjeto(campeonato);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Participantecampeonato>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Participanteevento> getParticipanteseventoEvento(Integer idEvento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipanteseventoEvento");
        mensaje.setIdConsulta(idEvento);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Participanteevento>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Participante> getParticipantesCampeonatoEvento(Campeonato campeonato) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipantesCampeonatoEvento");
        mensaje.setObjeto(campeonato);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Participante>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Participante> getParticipantesEvento(Integer idEvento) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipantesEvento");
        mensaje.setIdConsulta(idEvento);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Participante>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Participante> getParticipantesDeporte(Deporte deporte) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getParticipantesDeporte");
        mensaje.setObjeto(deporte);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Participante>)(Object)cliente.respuesta.getListaObjetos();

    }


    public List<Evento> getEventosDia(LocalDateTime day) throws NullPointerException{

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getEventosDia");
        mensaje.setDiaConsulta(day);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Evento>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Apuesta> getApuestasUsuario(int id) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getApuestasUsuario");
        mensaje.setIdConsulta(id);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Apuesta>)(Object)cliente.respuesta.getListaObjetos();


    }

    public List<Evento> getEventosTerminados() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getEventosTerminados");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Evento>)(Object)cliente.respuesta.getListaObjetos();

    }

    public List<Usuario> getRanking() {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("getRankingUsuarios");
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return (List<Usuario>)(Object)cliente.respuesta.getListaObjetos();

    }


    //LOGIN-----------------------

    public boolean iniciarSesion(Usuario usuario) {


        Mensaje mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("login");
        mensaje.setUsuario(usuario);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        this.sessionUser = cliente.respuesta.getUsuario();
        return cliente.respuesta.getComprobacion();

    }



    public boolean registrarUsuario(Usuario usuario) {

        mensaje = new Mensaje(sessionUser);
        mensaje.setConsulta("register");
        mensaje.setUsuario(usuario);
        cliente.enviarMensaje(mensaje);
        waitforResponse();
        return cliente.respuesta.getComprobacion();

    }


    private void waitforResponse() {
        this.terminado = false;
        synchronized(this){
            while(!terminado){
                try {
                    wait(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!terminado){
                    return;
                }
            }
        }
        return;
    }

    public void terminar(){
        synchronized(this){
            this.terminado = true;
            notifyAll();
        }
    }



}



