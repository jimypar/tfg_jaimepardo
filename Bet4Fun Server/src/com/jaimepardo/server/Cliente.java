package com.jaimepardo.server;

import com.jaimepardo.mapaDB.Mensaje;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Cliente {

    ControladorCliente controladorCliente;

    private ObjectInputStream entrada;
    private ObjectOutputStream salida;
    private Socket socket;

    private String server;
    private int port;

    Mensaje respuesta;

    private boolean conectado;

    public void setConectado(boolean conectado) {
        this.conectado = conectado;
    }

    //Constructor del cliente con el nombre del host,el puerto y la interfaz grafica.
    Cliente(String server, int port, ControladorCliente controladorCliente) {
        this.server = server;
        this.port = port;
        this.controladorCliente = controladorCliente;
    }

    //Metodo que crea el socket del cliente,la entrada, la salida y llama al hilo de escucha.
    public boolean iniciar() {
        try {
            socket = new Socket(server, port);
        }
        catch(Exception ec) {
            return false;
        }

        try{
            entrada = new ObjectInputStream(socket.getInputStream());
            salida = new ObjectOutputStream(socket.getOutputStream());
        }
        catch (IOException eIO) {
        }

        Escuchar escuchar = new Escuchar();
        escuchar.start();

        return true;
    }

    //Metodo que envia un mensaje al servidor.
    void enviarMensaje(Mensaje mensaje) {

        try {
            salida.writeObject(mensaje);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }





    //Hilo que esta escuchando la entrada de un mensaje y lo manda a la GUI
    class Escuchar extends Thread {

        public synchronized void run() {
            while(true) {
                try {
                    respuesta = (Mensaje) entrada.readObject();
                    controladorCliente.terminar();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
