package com.jaimepardo.util;

import com.jaimepardo.mapaDB.*;

import javax.swing.*;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Esta es una clase en la que tengo métodos estáticos para crear una ventana con un mensaje.
 * Cada método se refiere a un tipo distinto de mensaje.
 */
public class Util {

    private static final DecimalFormat df = new DecimalFormat("0.00");
    /**
     * Este método me muestra un mensaje de error con el texto recibido
     * @param message Texto del mensaje de error
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    /**
     * Este método me muestra un mensaje de aviso con el texto recibido
     * @param message Texto del mensaje de aviso
     */
    public static void showWarningAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Aviso", JOptionPane.WARNING_MESSAGE);
    }
    /**
     * Este método me muestra un mensaje de información con el texto recibido
     * @param message Texto del mensaje de información
     */
    public static void showInfoAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Informacion", JOptionPane.INFORMATION_MESSAGE);
    }

    public static String formatearFecha(LocalDate fechaMatriculacion) {
        DateTimeFormatter formateador =DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return formateador.format(fechaMatriculacion);
    }

    public static LocalDate parsearFecha(String fecha) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(fecha,formateador);
    }

    public static Object[] usuarioToRow(Usuario usuario) {
        Object[] fila =  new Object[]{usuario.getId(), usuario.getNombre(), usuario.getApellidos(),
                usuario.getUsername(), usuario.getPass(), usuario.getEmail(), usuario.getPuntos(), usuario.getRol()};
        return fila;
    }

    public static Object[] apuestaToRow(Apuesta apuesta) {
        Object[] fila =  new Object[]{apuesta.getId(), apuesta.getUsuario(), apuesta.getEvento(), apuesta.getApuesta()
        , apuesta.getCantidad(), apuesta.getGanada(), apuesta.getTerminada()};
        return fila;
    }

    public static Object[] eventoToRow(Evento evento) {
        Object[] fila =  new Object[]{evento.getId(), evento.getCampeonato().getDeporte(), evento.getCampeonato(), evento.getFecha(), evento.getTerminado()};
        return fila;
    }

    public static Object[] participanteToRow(Participante participante) {
        Object[] fila =  new Object[]{participante.getId(), participante.getDeporte(), participante.getNombre(), participante.getVictorias(), participante.getDerrotas(), participante.getEmpates(), participante.getLogo()};
        return fila;
    }

    public static Object[] campeonatoToRow(Campeonato campeonato) {
        Object[] fila =  new Object[]{campeonato.getId(), campeonato.getDeporte(), campeonato.getNombre(), campeonato.getAnno(), campeonato.getLogo()};
        return fila;
    }

    public static Object[] deporteToRow(Deporte deporte) {
        Object[] fila =  new Object[]{deporte.getId(), deporte.getNombre(), deporte.getNumParticipantes()};
        return fila;
    }

    public static Object[] rolToRow(Rol rol) {
        Object[] fila =  new Object[]{rol.getId(), rol.getDescripcion(), rol.getPermisoApuesta(), rol.getPermisoModificarPartidos(), rol.getPermisoModificarResultados(), rol.getPermisoModificarUsuarios()};
        return fila;
    }

    public static Byte checkToByte(JCheckBox checkBox) {
        if (checkBox.isSelected()){
            return 1;
        }else {
            return 0;
        }
    }

    public static boolean byteToCheck(Byte b) {
        if (b == 1){
            return true;
        }else {
            return false;
        }
    }

    public static String porcentajeToCuota(int porcentaje) {

        try {
            if (porcentaje<1)
                return df.format(100.0);

            double cuota = 100.0/porcentaje;
            return df.format(cuota);
        }catch (ArithmeticException a){
            return "1.00";
        }


    }

    public static int cuotaToPorcentaje(Double cuota) {

        if (cuota==100)
            return 1;
        return (int) (100/cuota);

    }

    public static Double textToDouble(String num) {
        num = num.replaceAll(",",".");
        return Double.parseDouble(num);
    }


}
