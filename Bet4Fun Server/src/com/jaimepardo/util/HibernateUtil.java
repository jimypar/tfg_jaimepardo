package com.jaimepardo.util;

import com.jaimepardo.mapaDB.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

  private static SessionFactory sessionFactory;
  private static Session session;


  /**
   * Crea la factoria de sesiones
   */
  public static void buildSessionFactory() {

    try {
      Configuration configuracion = new Configuration();
      //Cargo el fichero Hibernate.cfg.xml
      configuracion.configure("hibernate.cfg.xml");

      //Indico la clase mapeada con anotaciones
      configuracion.addAnnotatedClass(Apuesta.class);
      configuracion.addAnnotatedClass(Campeonato.class);
      configuracion.addAnnotatedClass(Deporte.class);
      configuracion.addAnnotatedClass(Evento.class);
      configuracion.addAnnotatedClass(Participante.class);
      configuracion.addAnnotatedClass(Participantecampeonato.class);
      configuracion.addAnnotatedClass(Participanteevento.class);
      configuracion.addAnnotatedClass(Rol.class);
      configuracion.addAnnotatedClass(Usuario.class);

      //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
      //Esta clase se usa para gestionar y proveer de acceso a servicios
      StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
              configuracion.getProperties()).build();

      //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
      sessionFactory = configuracion.buildSessionFactory(ssr);

      //vista.panel.setVisible(true);

    } catch (Exception e) {

      //ERROR CONEXION
      //vista.panel.setVisible(false);

    }

  }
	
  /**
   * Abre una nueva sesión
   */
  public static void openSession() {
    session = sessionFactory.openSession();
  }
	
  /**
   * Devuelve la sesión actual
   * @return
   */
  public static Session getCurrentSession() {
	
    if ((session == null) || (!session.isOpen()))
      openSession();

    return session;
  }
	
  /**
   * Cierra Hibernate
   */
  public static void closeSessionFactory() {
	
    if (session != null)
      session.close();
		
    if (sessionFactory != null)
      sessionFactory.close();
  }
}