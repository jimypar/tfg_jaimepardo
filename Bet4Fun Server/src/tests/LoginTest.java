package tests;

import com.jaimepardo.mapaDB.Usuario;
import com.jaimepardo.server.ControladorCliente;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LoginTest {


    @Test
    @DisplayName("Registrar Usuario")
    @Order(1)
    void RegistrarUsuario(){
        ControladorCliente controladorCliente = new ControladorCliente();
        Usuario usuario = new Usuario();
        usuario.setUsername("test");
        usuario.setPass("Passwordtest123");
        usuario.setNombre("a");
        usuario.setApellidos("a");
        usuario.setEmail("a");
        boolean result = controladorCliente.registrarUsuario(usuario);
        assertTrue(result);
    }

    @Test
    @DisplayName("Logear Usuario")
    @Order(2)
    void LogearUsuario() {
        ControladorCliente controladorCliente = new ControladorCliente();
        Usuario usuario = new Usuario();
        usuario.setUsername("test");
        usuario.setPass("Passwordtest123");
        boolean result = controladorCliente.iniciarSesion(usuario);
        assertTrue(result);
    }

    @Test
    @DisplayName("Logear Usuario con contraseña incorrecta")
    @Order(3)
    void LogearUsuarioFallo() {
        ControladorCliente controladorCliente = new ControladorCliente();
        Usuario usuario = new Usuario();
        usuario.setUsername("test");
        usuario.setPass("Passwordtest123FALLO");
        boolean result = controladorCliente.iniciarSesion(usuario);
        assertFalse(result);
    }

}