package tests;

import com.jaimepardo.server.ControladorCliente;
import com.jaimepardo.server.Servidor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServidorTest {

    @Test
    @DisplayName("Iniciar Server")
    void IniciarServer() {
        Servidor servidor = new Servidor(4444);
        assertEquals(true, servidor.online);
    }

    @Test
    @DisplayName("Iniciar Controlador")
    void IniciarControlador() {
        ControladorCliente controladorCliente = new ControladorCliente();
        assertEquals(true, controladorCliente.online);
    }

}