package tests;

import com.jaimepardo.mapaDB.*;
import com.jaimepardo.server.ControladorCliente;
import com.jaimepardo.server.Servidor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ConsultasTest {

    @Test
    @DisplayName("Buscar usuario")
    void BuscarUsuario() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Usuario user = controladorCliente.getUsuario(14);
        assertEquals(14,user.getId());
    }

    @Test
    @DisplayName("Buscar apuesta")
    void BuscarApuesta() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Apuesta apuesta = controladorCliente.getApuesta(14);
        assertEquals(14,apuesta.getId());
    }

    @Test
    @DisplayName("Buscar evento")
    void BuscarEvento() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Evento evento = controladorCliente.getEvento(14);
        assertEquals(14,evento.getId());
    }

    @Test
    @DisplayName("Buscar participante")
    void BuscarParticipante() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Participante participante = controladorCliente.getParticipante(14);
        assertEquals(14,participante.getId());
    }

    @Test
    @DisplayName("Buscar campeonato")
    void BuscarCampeonato(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Campeonato campeonato = controladorCliente.getCampeonato(4);
        assertEquals(4,campeonato.getId());
    }

    @Test
    @DisplayName("Buscar deporte")
    void BuscarDeporte(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Deporte deporte = controladorCliente.getDeporte(4);
        assertEquals(4,deporte.getId());
    }

    @Test
    @DisplayName("Buscar rol")
    void BuscarRol(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Rol rol = controladorCliente.getRol(4);
        assertEquals(4,rol.getId());
    }


    @Test
    @DisplayName("Buscar usuarios")
    void BuscarUsuarios() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getUsuarios());
    }

    @Test
    @DisplayName("Buscar eventos")
    void BuscarEventos() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getEventos());
    }

    @Test
    @DisplayName("Buscar participantes")
    void BuscarParticipantes() {
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getParticipantes());
    }

    @Test
    @DisplayName("Buscar campeonatos")
    void BuscarCampeonatos(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getCampeonatos());
    }

    @Test
    @DisplayName("Buscar deportes")
    void BuscarDeportes(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getDeportes());
    }

    @Test
    @DisplayName("Buscar rol")
    void BuscarRols(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getRoles());
    }

    @Test
    @DisplayName("Buscar participante de campeonato")
    void BuscarParticipanteCampeonatos(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        Campeonato campeonato = controladorCliente.getCampeonato(4);
        assertNotEquals(null,controladorCliente.getParticipantesCampeonato(campeonato));
    }

    @Test
    @DisplayName("Buscar participante de evento")
    void BuscarParticipanteEvento(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getParticipanteseventoEvento(14));
    }

    @Test
    @DisplayName("Buscar participante de deporte")
    void BuscarParticipanteDeporte(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getParticipanteseventoEvento(1));
    }

    @Test
    @DisplayName("Buscar apuestas usuario")
    void BuscarApuestasUsuario(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getApuestasUsuario(1));
    }

    @Test
    @DisplayName("Buscar apuestas usuario")
    void BuscarRankingUsuarios(){
        ControladorCliente controladorCliente = new ControladorCliente();
        login(controladorCliente);
        assertNotEquals(null,controladorCliente.getRanking());
    }


    private void login(ControladorCliente controladorCliente) {
        Usuario usuario = new Usuario();
        usuario.setUsername("test");
        usuario.setPass("Passwordtest123");
        controladorCliente.iniciarSesion(usuario);
    }


}