CREATE DATABASE IF NOT EXISTS bet4fun;
--
USE bet4fun;
--
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) NOT NULL,
  `permisoApuesta` tinyint(1) DEFAULT NULL,
  `permisoModificarResultados` tinyint(1) DEFAULT NULL,
  `permisoModificarPartidos` tinyint(1) DEFAULT NULL,
  `permisoModificarUsuarios` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `pass` varchar(150) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `puntos` int(11) NOT NULL DEFAULT 1000,
  `id_rol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_ibfk_1` (`id_rol`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
--
CREATE TABLE `deporte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `num_participantes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
CREATE TABLE `campeonato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `anno` int(11) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `id_deporte` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `campeonato_ibfk_1` (`id_deporte`),
  CONSTRAINT `campeonato_ibfk_1` FOREIGN KEY (`id_deporte`) REFERENCES `deporte` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
--
CREATE TABLE `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `terminado` tinyint(1) DEFAULT NULL,
  `id_campeonato` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `evento_ibfk_1` (`id_campeonato`),
  CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`id_campeonato`) REFERENCES `campeonato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
--
CREATE TABLE `participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `victorias` int(11) DEFAULT NULL,
  `empates` int(11) DEFAULT NULL,
  `derrotas` int(11) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `id_deporte` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `participante_ibfk_1` (`id_deporte`),
  CONSTRAINT `participante_ibfk_1` FOREIGN KEY (`id_deporte`) REFERENCES `deporte` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
--
CREATE TABLE `participantecampeonato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `victorias` int(11) DEFAULT NULL,
  `empates` int(11) DEFAULT NULL,
  `derrotas` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `id_participante` int(11) DEFAULT NULL,
  `id_campeonato` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `participantecampeonato_ibfk_1` (`id_participante`),
  KEY `participantecampeonato_ibfk_2` (`id_campeonato`),
  CONSTRAINT `participantecampeonato_ibfk_1` FOREIGN KEY (`id_participante`) REFERENCES `participante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `participantecampeonato_ibfk_2` FOREIGN KEY (`id_campeonato`) REFERENCES `campeonato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
--
CREATE TABLE `participanteevento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resultado` int(11) DEFAULT NULL,
  `empate` tinyint(1) DEFAULT NULL,
  `victoria` tinyint(1) DEFAULT NULL,
  `id_participante` int(11) DEFAULT NULL,
  `id_evento` int(11) DEFAULT NULL,
  `cuota` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `participanteevento_ibfk_1` (`id_participante`),
  KEY `participanteevento_ibfk_2` (`id_evento`),
  CONSTRAINT `participanteevento_ibfk_1` FOREIGN KEY (`id_participante`) REFERENCES `participante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `participanteevento_ibfk_2` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
--
CREATE TABLE `apuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `id_apuesta` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `terminada` tinyint(1) DEFAULT 0,
  `ganada` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `apuesta_ibfk_1` (`id_usuario`),
  KEY `apuesta_ibfk_2` (`id_evento`),
  CONSTRAINT `apuesta_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `apuesta_ibfk_2` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ;
--


